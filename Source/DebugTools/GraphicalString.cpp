/*
 * This file implements a bitmapped font
 * renderer.
 *
 * Author:      Stuart Rudderham
 * Created:     April 5, 2013
 * Last Edited: April 5, 2013
 */

#include "DebugTools/GraphicalString.hpp"
#include "Graphics/Camera/Camera.hpp"
#include "DebugTools/Debug.hpp"
#include "Graphics/OpenGL/Shader/Text/TextShader.hpp"
#include <SFML/Window/Context.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <type_traits>

// Static variables
sf::Texture                     GraphicalString::Texture;
const unsigned int              GraphicalString::CharacterScreenSize  = 16;             // Each character is 10 pixels wide on the screen
const GLfloat                   GraphicalString::CharacterTextureSize = 1.0f / 16.0f;   // The font texture has 16 characters per row/column
GraphicalString::ResourceLoader GraphicalString::TextureLoader;

GraphicalString::ResourceLoader::ResourceLoader()
{
    // Make sure we have an OpenGL context
    sf::Context context;

    // Load the texture
    dbErrorCheck( GraphicalString::Texture.loadFromFile("Content/Images/font.png"), "Unable to load GraphicalString texture" );
    sf::Texture::bind( &GraphicalString::Texture );
}

template<>
void Model<GraphicalString::Vertex>::DefineVAOLayout()
{
    // For explanation of use of sizeof() and offsetof() see -> http://www.opengl.org/wiki/Vertex_Array_Objects#Vertex_buffer_offset_and_stride

    // Vertex position data is stored at location 0
    SetAttribute(
        0,                                                                  // Location
        2,                                                                  // Size
        GL_FLOAT,                                                           // Type
        GL_FALSE,                                                           // Normalized?
        sizeof( GraphicalString::Vertex ),                                  // Stride
        reinterpret_cast<void*>( offsetof(GraphicalString::Vertex, pos) )   // Array buffer offset
    );

    // Texture coordinate data is stored at location 1
    SetAttribute(
        1,                                                                  // Location
        2,                                                                  // Size
        GL_FLOAT,                                                           // Type
        GL_FALSE,                                                           // Normalized?
        sizeof( GraphicalString::Vertex ),                                  // Stride
        reinterpret_cast<void*>( offsetof(GraphicalString::Vertex, tex) )   // Array buffer offset
    );
}

// ctor
GraphicalString::GraphicalString()
{
}

// Draw
void GraphicalString::Draw( const Camera& camera )
{
    static TextShader shader(
        "Source/Graphics/OpenGL/Shader/Text/TextShader.vert",
        "Source/Graphics/OpenGL/Shader/Text/TextShader.frag"
    );

    glm::vec2 startingPos( 10.0f, 10.0f );
    glm::vec2 pos = startingPos;
    glm::vec2 tex;

    model.SetExpectedQuadNum( str.length() );

    // For each character in the string
    for( unsigned int i = 0; i < str.length(); ++i ) {

        // If hit a newline then move down and back to the left
        if( str[i] == '\n' ) {
            pos.x = startingPos.x;
            pos.y += CharacterScreenSize;
            continue;
        }

        // Calculate texture coords based on letter
        tex.x = (str[i] % 16) * CharacterTextureSize;
        tex.y = (str[i] / 16) * CharacterTextureSize;

        // The texture coordinates are reversed because in OpenGL (0, 0) is the bottom-left.
        // To get the text to draw from the top down we flip the orthographic projection and
        // reverse the texture coordinates so that everything is still readable

        //                | Right-Top-Back              | Right-Bottom-Back           | Left-Bottom-Back | Left-Top-Back                |
        GLfloat posX[4] = { pos.x + CharacterScreenSize , pos.x + CharacterScreenSize , pos.x            , pos.x                        };
        GLfloat posY[4] = { pos.y + CharacterScreenSize , pos.y                       , pos.y            , pos.y + CharacterScreenSize  };
        GLfloat texX[4] = { tex.x + CharacterTextureSize, tex.x + CharacterTextureSize, tex.x            , tex.x                        };
        GLfloat texY[4] = { tex.y + CharacterTextureSize, tex.y                       , tex.y            , tex.y + CharacterTextureSize };

        model.AddQuad( posX, posY, texX, texY );

        pos.x += CharacterScreenSize;
    }

    // Copy data to GPU. Also clears the cpu buffer, so next
    // frame has a clean slate
    model.CopyToVBO( GL_DYNAMIC_DRAW );

    // Bind shader and texture
    shader.Bind();
    sf::Texture::bind( &GraphicalString::Texture );

    glm::mat4 MVP;

    // Draw the shadow first
    const glm::vec3 shadowOffset( 2.0f, 2.0f, 0.0f );

    glm::mat4 modelMat( 1.0f );
    modelMat = glm::translate( modelMat, shadowOffset );

    // FIXME: Add a ModelMatrix to all Models???
    //
    MVP = camera.GetViewProjectionMatrix() * modelMat;
    shader.SetMVPMatrix( MVP );
    shader.SetTextColour( glm::vec3(0.25f, 0.25f, 0.25f) );   // dark grey

    model.Draw();

    // Then draw the actual text
    MVP = camera.GetViewProjectionMatrix();
    shader.SetMVPMatrix( MVP );
    shader.SetTextColour( glm::vec3(1.0f, 1.0f, 1.0f) );   // white

    model.Draw();

    // Unbind the everything
    sf::Texture::bind( nullptr );
    shader.Unbind();
}
