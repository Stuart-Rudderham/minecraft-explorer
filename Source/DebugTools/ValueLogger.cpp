/*
 * This header implements a circular buffer for storing
 * numbers. When a new value is added an old value
 * is discarded. Also provided stat functions (e.g.
 * to get the average of all the numbers)
 *
 * Author:      Stuart Rudderham
 * Created:     August 21, 2012
 * Last Edited: May 18, 2013
 */

#include "DebugTools/ValueLogger.hpp"
#include "BasicTypes.hpp"
#include <algorithm>
#include <sstream>

// Constructor
ValueLogger::ValueLogger( const char* loggerName )
    : name( loggerName )
    ,  numValuesAdded( 0u )
    ,  average( std::numeric_limits<float>::quiet_NaN() )
    ,  variance( std::numeric_limits<float>::quiet_NaN() )
    ,  summedValues( 0 )
    ,  summedSquaredValues( 0 )
    ,  minimumValue( std::numeric_limits<Int64>::max() )
    ,  maximumValue( std::numeric_limits<Int64>::min() )
{

}

// add a new value to the logger
void ValueLogger::AddNewValue( Int64 newValue )
{
    // update bookkeeping
    numValuesAdded++;

    // Update the min/max values if necessary
    minimumValue = std::min( minimumValue, newValue );
    maximumValue = std::max( maximumValue, newValue );

    // Add the new value to the buffer
    buffer.push_back( newValue );

    // and update the intermediate calculations with the new value
    summedValues += newValue;
    summedSquaredValues += newValue * newValue;

    // if the buffer is full then we have to remove an old value
    if( buffer.size() > MaxBufferSize ) {
        Int64 oldValue = buffer.front();

        summedValues -= oldValue;
        summedSquaredValues -= oldValue * oldValue;

        buffer.pop_front();

        // If the element we removed was the max/min element then we need to find the new max/min
        if( oldValue == minimumValue ) {
            minimumValue = *std::min_element( buffer.begin(), buffer.end() );
        }

        if( oldValue == maximumValue ) {
            maximumValue = *std::max_element( buffer.begin(), buffer.end() );
        }
    }

    // Recalculate stats with new value
    float numElements = static_cast<float>( buffer.size() );            // cast to float to make sure division is always using floating point, not ints

    average = summedValues / numElements;

    variance = ( summedSquaredValues - ( (summedValues * summedValues) / numElements ) ) / numElements;
}
