/*
 * This header defines all the variables, functions, and macros that
 * are used for debugging purposes
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: May 5, 2013
 */

#ifndef DEBUGTOOLS_HPP_INCLUDED
#define DEBUGTOOLS_HPP_INCLUDED

#include <glm/glm.hpp>  // GLM 0.9.5 has a forward declaration file "glm/fwd.hpp" so we won't have to include everything
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

class Camera;

#define ENABLE_ASSERTS              1
#define ENABLE_WARNINGS             1
#define ENABLE_ERROR_CHECKING       1
#define ENABLE_CONSOLE_PRINTING     1
#define ENABLE_SCREEN_PRINTING      1

// Macros to turn off debugging in release

#if ENABLE_ASSERTS
    #define dbAssert( condition, message, ... )     Debug::Test( condition, true,  __FILE__, __LINE__, __PRETTY_FUNCTION__, #condition, message, ##__VA_ARGS__ )
#else
    #define dbAssert( condition, message, ... )
#endif


#if ENABLE_WARNINGS
    #define dbWarn( condition, message, ... )       Debug::Test( condition, false, __FILE__, __LINE__, __PRETTY_FUNCTION__, #condition, message, ##__VA_ARGS__ )
#else
    #define dbWarn( condition, message, ... )
#endif


#if ENABLE_ERROR_CHECKING
    #define dbErrorCheck( condition, message, ... ) Debug::Test( condition, true,  __FILE__, __LINE__, __PRETTY_FUNCTION__, #condition, message, ##__VA_ARGS__ )
#else
    #define dbErrorCheck( condition, message, ... ) (condition)
#endif

#if ENABLE_CONSOLE_PRINTING
    #define dbPrintToConsole( message, ... )        Debug::Print( std::cerr, message, ##__VA_ARGS__ )
#else
    #define dbPrintToConsole( message, ... )
#endif


#if ENABLE_SCREEN_PRINTING
    #define dbPrintToScreen( message, ... )         Debug::Print( debugString, message, ##__VA_ARGS__ )
    #define dbDrawDebugText( pos )                  Debug::DrawDebugText( pos )
#else
    #define dbPrintToScreen( message, ... )
    #define dbDrawDebugText( target, ... )
#endif

#define dbCrash( message, ... )                     Debug::Test( false, true, __FILE__, __LINE__, __PRETTY_FUNCTION__, "Crashing", message, ##__VA_ARGS__ )
#define dbPause( message )                          Debug::Pause( message );

extern std::stringstream debugString;

namespace Debug
{
    // A function to print string to an output stream
    inline void Print( std::ostream& os )                               // Base case
    {
        os << std::endl;                                                // only print out a newline
    }

    template<typename T, typename... Args>
    inline void Print( std::ostream& os, T& value, Args&&... args )     // Recursive case
    {
        os << std::noskipws;                                            // don't skip leading whitespace
        os << std::fixed << std::setprecision( 2 );             		// print out 2 digits after the decimal place
        os << std::boolalpha;                                           // print True/False instead of 1/0

        os << value;
        Print( os, args... );
    }

    // A function that inserts a breakpoint into the program
    inline void Pause( const char* message )
    {
        Debug::Print( std::cerr, "PAUSE - ", message );

        // Add a breakpoint
        __asm__( "int $0x3" );
    }

    // A function to crash the program with hooks for a debugger
    inline void Crash()
    {
        Debug::Pause( "CRASHING" );     // Add a breakpoint
        exit( 1 );                      // Then DIE
    }

    // A custom assert function. Triggers if the condition is false
    // Shouldn't really be called directly, use the macro dbAssert or
    // dbWarn to automatically fill out the correct file, line number, etc...
    template<typename... Args>
    void Test( bool condition, bool crashIfFail, const char* file, int line, const char* function, const char* conditionString, const char* message, Args&&... args )
    {
        // If the assert triggers print out an error message
        if( condition == false ) {
            Debug::Print( std::cerr, "ASSERT" );
            Debug::Print( std::cerr, "    Message:   ", message, args... );
            Debug::Print( std::cerr, "    File:      ", file             );
            Debug::Print( std::cerr, "    Line:      ", line             );
            Debug::Print( std::cerr, "    Function:  ", function         );
            Debug::Print( std::cerr, "    Condition: ", conditionString  );
            Debug::Print( std::cerr, "" );

            // Then crash the program (if desired)
            if( crashIfFail ) {
                Debug::Crash();
            }
        }
    }

    // A function to draw the debug text onto the screen
    void DrawDebugText( const Camera& camera );
}

// These functions overloads the "<<" operator to allow you to print out glm::vec2, glm::vec3, and glm::vec4
std::ostream& operator<<( std::ostream& os, const glm::vec2  vec );
std::ostream& operator<<( std::ostream& os, const glm::ivec2 vec );
std::ostream& operator<<( std::ostream& os, const glm::vec3  vec );
std::ostream& operator<<( std::ostream& os, const glm::ivec3 vec );
std::ostream& operator<<( std::ostream& os, const glm::vec4  vec );

#endif // DEBUGTOOLS_HPP_INCLUDED
