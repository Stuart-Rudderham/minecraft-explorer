/*
 * This file implements of all the variables, functions
 * and macros that are used for debugging purposes
 *
 * Author:      Stuart Rudderham
 * Created:     January 29, 2012
 * Last Edited: May 5, 2013
 */

#include "DebugTools/Debug.hpp"
#include "DebugTools/GraphicalString.hpp"

// The string that all debugging text is added to
std::stringstream debugString;

void Debug::DrawDebugText( const Camera& camera )
{
    // Make this static because we don't want to keep creating
    // and destroying VBOs and VAOs every frame
    static GraphicalString graphicalString;

    graphicalString.SetString( debugString.str() );
    debugString.str("");

    graphicalString.Draw( camera );
}

std::ostream& operator<<( std::ostream& os, const glm::vec2 vec )
{
    os << "(" << vec.x << ", " << vec.y << ")";
    return os;
}

std::ostream& operator<<( std::ostream& os, const glm::ivec2 vec )
{
    os << "(" << vec.x << ", " << vec.y << ")";
    return os;
}

std::ostream& operator<<( std::ostream& os, const glm::vec3 vec )
{
    os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
    return os;
}

std::ostream& operator<<( std::ostream& os, const glm::ivec3 vec )
{
    os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
    return os;
}

std::ostream& operator<<( std::ostream& os, const glm::vec4 vec )
{
    os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
    return os;
}
