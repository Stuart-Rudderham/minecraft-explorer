/*
 * This header defines a bitmapped font
 * renderer.
 *
 * Author:      Stuart Rudderham
 * Created:     April 5, 2013
 * Last Edited: April 5, 2013
 */

#ifndef GRAPHICALSTRING_HPP_INCLUDED
#define GRAPHICALSTRING_HPP_INCLUDED

#include "Graphics/Model/Model.hpp"
#include <string>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <glm/glm.hpp>

class Camera;

class GraphicalString {
    public:
        GraphicalString();                                      // ctor
        ~GraphicalString() = default;                           // default dtor

        inline void SetString( const std::string& s ) {         // Setter for the string
            str = s;
        }

        void Draw( const Camera& camera );                      // Draw the string with the top-left corner located at the given screen coords

        struct Vertex {                                         // A struct defining the layout of a single vertex.
            GLfloat pos[2];
            GLfloat tex[2];

            Vertex( GLfloat x, GLfloat y, GLfloat u, GLfloat v )
                : pos{x, y}
                , tex{u, v}
            { }
        };

    private:
        static sf::Texture Texture;                             // The texture shared by all strings that can be drawn to the screen
        static const unsigned int CharacterScreenSize;          // The size of each character drawn onto the screen (in pixels)
        static const GLfloat CharacterTextureSize;              // The size of each character in texture coords (i.e. within [0, 1])

        static struct ResourceLoader {                          // This object will get constructed at program start and
            ResourceLoader();                                   // it's constructor will automatically load the texture
        } TextureLoader;                                        // for the on-screen font

        std::string str;                                        // The text of the GraphicalString

        Model<GraphicalString::Vertex> model;                   // The vertex data for the textured quads that
                                                                // will be drawn to the screen
};

#endif // GRAPHICALSTRING_HPP_INCLUDED
