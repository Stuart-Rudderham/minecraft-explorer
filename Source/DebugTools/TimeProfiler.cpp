/*
 * This file implements a class that can
 * be used to profile how long something
 * takes
 *
 * Author:      Stuart Rudderham
 * Created:     February 9, 2012
 * Last Edited: Mey 18, 2013
 */

#include "DebugTools/TimeProfiler.hpp"

TimeProfiler::TimeProfiler( const char* profilerName )
    : timeValues( profilerName )
{

}

// call this at the start of the section you want to profile
void TimeProfiler::Start() {
    timer.restart();
}

// call this at the end of the section you want to profile
void TimeProfiler::Stop() {
    timeValues.AddNewValue( timer.getElapsedTime().asMicroseconds() );              // get the elapsed time from the timer and add it to the buffer
}

// Print stats to the screen
// All times are converted from microseconds to milliseconds
void TimeProfiler::PrintStatsToScreen() {
    dbPrintToScreen( "", timeValues.GetName().c_str()                                            );
    dbPrintToScreen( "\tFPS: ",        1000000.0f / timeValues.GetAverage()                      );
    dbPrintToScreen( "\tCurrent: ",    timeValues.GetMostRecentValue() / 1000.0f                 );
    dbPrintToScreen( "\tAverage: ",    timeValues.GetAverage()         / 1000.0f                 );
    dbPrintToScreen( "\tVariance: ",   timeValues.GetVariance()        / 1000 /* Leave as int */ );
    dbPrintToScreen( "\tMin: ",        timeValues.GetMinimumValue()    / 1000 /* Leave as int */ );
    dbPrintToScreen( "\tMax: ",        timeValues.GetMaximumValue()    / 1000 /* Leave as int */ );
    dbPrintToScreen( "\tNum Frames: ", timeValues.GetNumValuesAdded()                            );
}
