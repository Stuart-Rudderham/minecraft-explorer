/*
 * This header defines a circular buffer for storing
 * numbers. When a new value is added an old value
 * is discarded. Also provided stat functions (e.g.
 * to get the average of all the numbers)
 *
 * Author:      Stuart Rudderham
 * Created:     August 21, 2012
 * Last Edited: May 18, 2013
 */

#ifndef VALUELOGGER_HPP_INCLUDED
#define VALUELOGGER_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include <SFML/Config.hpp>
#include <deque>

class ValueLogger {
    private:
        static const unsigned int MaxBufferSize = 120;

        std::deque<sf::Int64> buffer;                                               // the buffer

        std::string name;                                                           // a decription of what the logger is logging (e.g. "Global Framerate")
        sf::Int64 graphScale;                                                       // the max value displayable in the graph

        unsigned int numValuesAdded;                                                // the total number of values that have been added to the buffer

        float average;                                                              // Stat variables. Average is an arithmetic average
        float variance;

        sf::Int64 summedValues;                                                     // these are intermediate values used to calculate the stats
        sf::Int64 summedSquaredValues;

        sf::Int64 minimumValue;                                                     // min/max value in the buffer
        sf::Int64 maximumValue;

    public:
        ValueLogger( const char* loggerName );                                      // ctor

        void AddNewValue( sf::Int64 newValue );                                     // add a new value to the logger

        inline const std::string& GetName() const { return name; }

        // Get a value from the buffer
        inline sf::Int64 GetValueAt( unsigned int i ) const {
            dbAssert( i < buffer.size(), "Tried to get value at invalid index" );
            return buffer[i];
        }

        // Getters for the stats
        inline sf::Int64    GetMostRecentValue() const { return buffer.empty() ? std::numeric_limits<sf::Int64>::quiet_NaN() : buffer.back(); }
        inline unsigned int GetNumValuesAdded()  const { return numValuesAdded; }
        inline float        GetAverage()         const { return average;        }
        inline float        GetVariance()        const { return variance;       }
        inline sf::Int64    GetMinimumValue()    const { return minimumValue;   }
        inline sf::Int64    GetMaximumValue()    const { return maximumValue;   }
};

#endif // VALUELOGGER_HPP_INCLUDED
