/*
 * This header defines a class that can
 * be used to profile how long something
 * takes
 *
 * Author:      Stuart Rudderham
 * Created:     February 9, 2012
 * Last Edited: November 22, 2012
 */

#ifndef TIMEPROFILER_HPP_INCLUDED
#define TIMEPROFILER_HPP_INCLUDED

#include "DebugTools/ValueLogger.hpp"
#include <SFML/System/Clock.hpp>

class TimeProfiler {
    private:
        sf::Clock timer;                                                            // a clock used to keep track of time
        ValueLogger timeValues;                                                     // the buffer used to hold the values

    public:
        TimeProfiler( const char* profilerName );

        void Start();                                                               // call this to start timing things
        void Stop();                                                                // call this to stop

        void PrintStatsToScreen();                                                  // sends the stats to the debug string

        inline sf::Int64 GetMostRecentTime() const {                                // get the most recent time recorded
            return timeValues.GetMostRecentValue();
        }
};

#endif // TIMEPROFILER_HPP_INCLUDED
