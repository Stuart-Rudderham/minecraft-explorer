/*
 * This header defines wrapped classes around
 * 2D and 3D points to specify what coordinate
 * system the points are relative to
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 13, 2013
 */

#ifndef COODINATESYSTEM_HPP_INCLUDED
#define COODINATESYSTEM_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include <glm/glm.hpp>
#include <cmath>

// The regular mod function (%) doesn't work how we would like for negative numbers
// This function has the correct behaviour
//     (x + 1) mod x = 1
//     (-1)    mod x = x - 1
// FIXME: This should probably be in its own file, since it's not only used in the corrdinate system
inline int CorrectMod( int value, int max ) {
    int modulus = value % max;                                      // Take the mod
    return modulus + (modulus < 0 ? max : 0);                       // Adjust the value if negative
}

inline float CorrectMod( float value, float max ) {
    float modulus = fmod(value, max);                               // Take the mod
    return modulus + (modulus < 0.0f ? max : 0.0f);                 // Adjust the value if negative
}

// FIXME: Replace with inheriting constructors when upgrade compiler
// FIXME: Could do conversion directly rather than stuff like ToBlock().ToChunk().ToRegion();

// An interactive calculator for this can be found here -> http://dinnerbone.com/minecraft/tools/coordinates/

// These are in reverse order of granularity so that the conversion functions have
// the full definitions of the types they need

/*
 * The position of a Region in the XZ plane of the World
 * Moving 1 unit of distance is equivalent to
 * moving 512 units of distance in WorldCoords
 */
struct RegionCoord : public glm::ivec2 {
    RegionCoord( glm::ivec2 p )
        : glm::ivec2( p )
    {}

    RegionCoord( int x, int y )
        : glm::ivec2( x, y )
    {}
};

/*
 * The position of a Chunk in the XZ plane of the World
 * Moving 1 unit of distance is equivalent to
 * moving 16 units of distance in WorldCoords
 */
struct ChunkCoord : public glm::ivec2 {
    ChunkCoord( glm::ivec2 p )
        : glm::ivec2( p )
    {}

    ChunkCoord( int x, int y )
        : glm::ivec2( x, y )
    {}

    RegionCoord ToRegion() {
        return RegionCoord(
            std::floor(x / 32.0f),
            std::floor(y / 32.0f)
        );
    }
};

/*
 * The position of a Chunk within a Region
 *     x is in the range [0, 31]
 *     y is in the range [0, 31]
 */
struct LocalChunkCoord : public glm::ivec2 {
    LocalChunkCoord( ChunkCoord chunkCoord, RegionCoord regionCoord )
    {
        dbAssert( chunkCoord.ToRegion() == regionCoord, "Cannot convert, Chunk doesn't belong to this Region" );

        x = CorrectMod( chunkCoord.x, 32 );
        y = CorrectMod( chunkCoord.y, 32 );

        dbAssert( x >= 0 && x < 32, "Invalid value, LocalChunkCoord.x = ", x );
        dbAssert( y >= 0 && y < 32, "Invalid value, LocalChunkCoord.y = ", y );
    }
};

/*
 * Any integer 3D point in the world
 * Moving 1 unit of distance is equivalent to
 * moving 1 unit of distance in WorldCoords
 */
struct BlockCoord : public glm::ivec3 {
    BlockCoord( glm::ivec3 p )
        : glm::ivec3( p )
    {}

    BlockCoord( int x, int y, int z )
        : glm::ivec3( x, y, z )
    {}

    ChunkCoord ToChunk() {
        return ChunkCoord(
            std::floor(x / 16.0f),
            std::floor(z / 16.0f)
        );
    }

    RegionCoord ToRegion() {
        return ToChunk().ToRegion();
    }
};

/*
 * The position of a Block within a Chunk
 *     x is in the range [0, 15]
 *     y is in the range [0, 255]
 *     z is in the range [0, 15]
 */
struct LocalBlockCoord : public glm::ivec3 {
    LocalBlockCoord( BlockCoord blockCoord, ChunkCoord chunkCoord )
    {
        dbAssert( blockCoord.ToChunk() == chunkCoord, "Cannot convert, Block doesn't belong to this Chunk" );

        x = CorrectMod( blockCoord.x, 16  );
        y = CorrectMod( blockCoord.y, 256 );
        z = CorrectMod( blockCoord.z, 16  );

        dbAssert( x >= 0 && x < 16 , "Invalid value, LocalBlockCoord.x = ", x );
        dbAssert( y >= 0 && y < 256, "Invalid value, LocalBlockCoord.y = ", y );
        dbAssert( z >= 0 && z < 16 , "Invalid value, LocalBlockCoord.z = ", z );
    }
};

/*
 * Any 3D point in the world
 */
struct WorldCoord : public glm::vec3 {
    WorldCoord( glm::vec3 p )
        : glm::vec3( p )
    {}

    WorldCoord( float x, float y, float z )
        : glm::vec3( x, y, z )
    {}

    BlockCoord ToBlock() {
        return BlockCoord(
            std::floor(x),
            std::floor(y),
            std::floor(z)
        );
    }

    ChunkCoord ToChunk() {
        return ToBlock().ToChunk();
    }

    RegionCoord ToRegion() {
        return ToBlock().ToChunk().ToRegion();
    }

    LocalBlockCoord ToLocalBlock() {
        return LocalBlockCoord(
            ToBlock(),
            ToChunk()
        );
    }

    LocalChunkCoord ToLocalChunk() {
        return LocalChunkCoord (
            ToChunk(),
            ToRegion()
        );
    }
};

// Needed so that RegionCoords can be used as the key in an std::unordered_map
// See -> http://stackoverflow.com/questions/9047612/glmivec2-as-key-in-unordered-map
// And -> http://stackoverflow.com/questions/8157937/how-to-specialize-stdhashkeyoperator-for-user-defined-type-in-unordered
namespace std {
    template<>
    struct hash<RegionCoord> {
        size_t operator()(const RegionCoord& c) const {
            return std::hash<int>()(c.x) ^ std::hash<int>()(c.y);
        }
    };
}

// Make sure the child classes don't have any members or vtables that
// their parent objects don't. This way we can pass them by value to
// functions that take glm::vecs without worrying about object slicing
static_assert( sizeof( RegionCoord     ) == sizeof( glm::ivec2 ), "Do not add members to RegionCoord"     );
static_assert( sizeof( ChunkCoord      ) == sizeof( glm::ivec2 ), "Do not add members to ChunkCoord"      );
static_assert( sizeof( LocalChunkCoord ) == sizeof( glm::ivec2 ), "Do not add members to LocalChunkCoord" );
static_assert( sizeof( BlockCoord      ) == sizeof( glm::ivec3 ), "Do not add members to BlockCoord"      );
static_assert( sizeof( LocalBlockCoord ) == sizeof( glm::ivec3 ), "Do not add members to LocalBlockCoord" );
static_assert( sizeof( WorldCoord      ) == sizeof( glm::vec3  ), "Do not add members to WorldCoord"      );

#endif // COODINATESYSTEM_HPP_INCLUDED
