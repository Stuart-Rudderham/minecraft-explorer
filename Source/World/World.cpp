/*
 * This file implements a class representing
 * the entire game world. This includes things
 * like the map, skybox, and player.
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: June 8, 2013
 */

#include "World/World.hpp"
#include "World/Minecraft/Map.hpp"
#include "World/Skybox/Skybox.hpp"
#include "World/Player/Player.hpp"
#include <SFML/System/Clock.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Variables to define the day/night cycle
const int RealTimeDayLength = 100;         // 100 seconds
const int GameTimeDayLength = 86400;       // 60 seconds * 60 minutes * 24 hours

const int TransitionLength  = 3600 * 2;    // 60 seconds * 60 minutes * 2 hours

const int Midnight = 0;                    // 12:00AM  Moon is right above Player
const int Dawn     = 21600;                // 6:00AM   Sun/Moon is horizontal with Player
const int Noon     = 43200;                // 12:00PM  Sun is right above Player
const int Dusk     = 64800;                // 6:00PM   Sun/Moon is horizontal with Player

const int SunriseBegin = Dawn - TransitionLength / 2;
const int SunriseEnd   = Dawn + TransitionLength / 2;

const int SunsetBegin  = Dusk - TransitionLength / 2;
const int SunsetEnd    = Dusk + TransitionLength / 2;

const glm::vec3 initialLightDir = glm::cross(sunRotationAxis, glm::vec3(0, 1, 0));

const glm::vec3 daySkyColour(           // Light blue
    115.0f / 255.0f,
    180.0f / 255.0f,
    255.0f / 255.0f
);

const glm::vec3 nightSkyColour(         // Very dark blue, pretty much black
    0.0f   / 255.0f,
    0.0f   / 255.0f,
    10.0f  / 255.0f
);

const glm::vec3 transitionSkyColour(    // Orange
    255.0f / 255.0f,
    140.0f / 255.0f,
    0.0f   / 255.0f
);

// FIXME: These should probably go in their own file
// Do linear interpolation between the two extremes
template<typename T>
inline T Lerp( const T min, const float current, const T max ) {
    return static_cast<T>( min + ( (max - min) * current ) );
}

// Do linear interpolation between the two extremes
template<typename T>
inline T SmoothLerp( const T min, const float current, const T max ) {
    return Lerp( min, (current * current) * ( 3.0f - (2.0f * current) ), max );
}

/*
 * Constructor
 */
World::World( std::string worldName, Player& p )
    : name( worldName )
    , viewDistance( 10 * 16 /* FIXME */ )
    , timeOfDay( Dawn )
    , player( p )
    , map( worldName, player )
    , skybox()

{
    player.MoveBy( glm::vec3(0, 60, 0) );
}

/*
 * Change the current game time by the given number of seconds
 */
void World::AdjustTimeOfDay( int delta )
{
    timeOfDay += delta;
    timeOfDay = CorrectMod( timeOfDay, GameTimeDayLength );
}

/*
 * Update the World based on the amount of time that has passed since the last update
 */
void World::Update( float deltaT )
{
    // Update the map, loading/unloading Chunks
    map.Update( deltaT );

    // Resolve any collisions here

    // Update the World time
    AdjustTimeOfDay( GameTimeDayLength * (deltaT / RealTimeDayLength) );

    // Update light direction and intensity
    sunAngle = Lerp( 0.0f, timeOfDay / (float)GameTimeDayLength, 360.0f ) - 90.0f;

    glm::mat4 rotMat(1.0f);
    rotMat = glm::rotate(rotMat, sunAngle, sunRotationAxis);
    glm::vec4 t = -glm::normalize( rotMat * glm::vec4(initialLightDir, 0) );
    sunlightDir = glm::vec3( t.x, t.y, t.z );

    // Calculate how strong the light is, based on the current time
    float minStrength = 0.0f;
    float maxStrength = 1.0f;

    dbPrintToScreen("SunriseBegin: ", SunriseBegin);
    dbPrintToScreen("SunriseEnd  : ", SunriseEnd  );
    dbPrintToScreen("SunsetBegin : ", SunsetBegin );
    dbPrintToScreen("SunsetEnd   : ", SunsetEnd   );

    // FIXME: Make sunrise/set more pretty
    // Could modify sides of skybox independently (i.e. orange on
    // the side with the setting/rising sun and black on the side
    // with the moon, with a nice gradient on the other sides)

    // Sunrise
    if( timeOfDay >= SunriseBegin && timeOfDay <= SunriseEnd ) {
        float lerpFactor = (timeOfDay - SunriseBegin) / (float)TransitionLength;

        lightIntensity = SmoothLerp( minStrength, lerpFactor, maxStrength );
        //skyColour = SmoothLerp( nightSkyColour, lerpFactor, daySkyColour );

        if( lerpFactor < 0.5f ) {
            skyColour = SmoothLerp( nightSkyColour, lerpFactor * 2, transitionSkyColour );
        } else {
            skyColour = SmoothLerp( transitionSkyColour, (lerpFactor - 0.5f) * 2, daySkyColour );
        }

        dbPrintToScreen( "Sunrise" );
    }
    // Day
    else if( timeOfDay > SunriseEnd && timeOfDay < SunsetBegin ) {
        lightIntensity = maxStrength;
        skyColour = daySkyColour;

        dbPrintToScreen( "Day" );
    }
    // Sunset
    else if( timeOfDay >= SunsetBegin && timeOfDay <= SunsetEnd ) {
        float lerpFactor = (timeOfDay - SunsetBegin) / (float)TransitionLength;

        lightIntensity = SmoothLerp( maxStrength, lerpFactor, minStrength );
        //skyColour = SmoothLerp( daySkyColour, lerpFactor, nightSkyColour );

        if( lerpFactor < 0.5f ) {
            skyColour = SmoothLerp( daySkyColour, lerpFactor * 2, transitionSkyColour );
        } else {
            skyColour = SmoothLerp( transitionSkyColour, (lerpFactor - 0.5f) * 2, nightSkyColour );
        }

        dbPrintToScreen( "Sunset" );
    }
    // Night
    else {
        lightIntensity = minStrength;
        skyColour = nightSkyColour;

        dbPrintToScreen( "Night" );
    }

    // FIXME: Covert to AM and PM???
    dbPrintToScreen(
          "Time: "
        , timeOfDay
        , " ("
        , timeOfDay / 3600
        , ":"
        , (timeOfDay % 3600) / 60
        // , timeOfDay > Noon ? "PM" : "AM"
        , ")"
    );
    dbPrintToScreen( "Sun Angle: ", sunAngle );
    dbPrintToScreen( "Sun Intensity: ", lightIntensity );
}

/*
 * Draw the World centered around the Player from the perspective of the given Camera
 */
void World::Draw( const Camera& camera )
{
    glEnable(GL_BLEND);
    skybox.Draw( camera, player.GetPosition(), sunAngle, skyColour );

    map.Draw( camera, sunlightDir, lightIntensity );
    glDisable(GL_BLEND);
}
