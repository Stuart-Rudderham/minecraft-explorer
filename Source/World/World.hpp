/*
 * This header defines a class representing
 * the entire game world. This includes things
 * like the map, skybox, and player.
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: June 8, 2013
 */

#ifndef WORLD_HPP_INCLUDED
#define WORLD_HPP_INCLUDED

#include "World/Player/Player.hpp"
#include "Graphics/Camera/Camera.hpp"
#include "World/Minecraft/Map.hpp"
#include "World/Skybox/Skybox.hpp"

#include <string>
#include <glm/glm.hpp>

 // Forward declarations
class Map;
class Skybox;
class Player;
class Camera;

// FIXME: this is silly
const glm::vec3 sunRotationAxis = glm::vec3(0, 0, 1);

class World {
    public:
        World( std::string worldName, Player& p );

        void Update( float deltaT );
        void Draw( const Camera& camera );

        void AdjustTimeOfDay( int delta );

    private:
        std::string name;

        float viewDistance;
        int timeOfDay;
        glm::vec3 skyColour;
        float sunAngle;
        glm::vec3 sunlightDir;
        float lightIntensity;

        Player& player;

        Map map;

        Skybox skybox;
        //Camera sunCamera;               // The Camera used to draw the Map from the perspective of the sun (for shadow mapping)
};

#endif // WORLD_HPP_INCLUDED
