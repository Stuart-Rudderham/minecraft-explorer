/*
 * This file implements a skybox that
 * encloses the entire Minecraft world
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: June 8, 2013
 */

#include "World/Skybox/Skybox.hpp"
#include "World/World.hpp"
#include "Graphics/OpenGL/Shader/Skybox/SkyboxShader.hpp"
#include "Graphics/Camera/Camera.hpp"
#include <SFML/Window/Context.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <type_traits>

// Static variables
sf::Texture            Skybox::SkyTexture;
sf::Texture            Skybox::SunMoonTexture;
Skybox::ResourceLoader Skybox::Loader;

Skybox::ResourceLoader::ResourceLoader()
{
    // Make sure we have an OpenGL context
    sf::Context context;

    // Load the texture sheet
    dbErrorCheck( Skybox::SkyTexture.loadFromFile("Content/Images/skybox_grey.png"), "Unable to load Skybox texture" );
    dbErrorCheck( Skybox::SunMoonTexture.loadFromFile("Content/Images/SunMoon.png"), "Unable to load Skybox texture" );
}

template<>
void Model<Skybox::Vertex>::DefineVAOLayout()
{
    // For explanation of use of sizeof() and offsetof() see -> http://www.opengl.org/wiki/Vertex_Array_Objects#Vertex_buffer_offset_and_stride

    // Vertex position data is stored at location 0
    SetAttribute(
        0,                                                          // Location
        3,                                                          // Size
        GL_FLOAT,                                                   // Type
        GL_FALSE,                                                   // Normalized?
        sizeof( Skybox::Vertex ),                                   // Stride
        reinterpret_cast<void*>( offsetof(Skybox::Vertex, pos) )    // Array buffer offset
    );

    // Texture coordinate data is stored at location 1
    SetAttribute(
        1,                                                          // Location
        2,                                                          // Size
        GL_FLOAT,                                                   // Type
        GL_FALSE,                                                   // Normalized?
        sizeof( Skybox::Vertex ),                                   // Stride
        reinterpret_cast<void*>( offsetof(Skybox::Vertex, tex) )    // Array buffer offset
    );
}

Skybox::Skybox()
{
              skyboxModel.SetExpectedQuadNum( 6           );
    celestialObjectsModel.SetExpectedQuadNum( 1 + 1 + 100 ); // sun, moon, 100 stars

    const float size = 1.0f;
    const float sunSize = size / 16.0f;

    // Front Quad           | Right-Top-Front  | Right-Bottom-Front | Left-Bottom-Front  | Left-Top-Front    |
    GLfloat frontPosX[4]  = {  size            ,  size              , -size              , -size             };
    GLfloat frontPosY[4]  = {  size            , -size              , -size              ,  size             };
    GLfloat frontPosZ[4]  = {  size            ,  size              ,  size              ,  size             };
    GLfloat frontTexX[4]  = {  0.5f            ,  0.5f              ,  0.0f              ,  0.0f             };
    GLfloat frontTexY[4]  = {  0.0f            ,  0.5f              ,  0.5f              ,  0.0f             };

    skyboxModel.AddQuad( frontPosX, frontPosY, frontPosZ, frontTexX, frontTexY );

    // Back  Quad           | Left-Top-Back    | Left-Bottom-Back   | Right-Bottom-Back  | Right-Top-Back    |
    GLfloat backPosX[4]   = { -size            , -size              ,  size              ,  size             };
    GLfloat backPosY[4]   = {  size            , -size              , -size              ,  size             };
    GLfloat backPosZ[4]   = { -size            , -size              , -size              , -size             };
    GLfloat backTexX[4]   = {  0.5f            ,  0.5f              ,  0.0f              ,  0.0f             };
    GLfloat backTexY[4]   = {  0.0f            ,  0.5f              ,  0.5f              ,  0.0f             };

    skyboxModel.AddQuad( backPosX, backPosY, backPosZ, backTexX, backTexY );

    // Left  Quad           | Left-Top-Front   | Left-Bottom-Front  | Left-Bottom-Back   | Left-Top-Back     |
    GLfloat leftPosX[4]   = { -size            , -size              , -size              , -size             };
    GLfloat leftPosY[4]   = {  size            , -size              , -size              ,  size             };
    GLfloat leftPosZ[4]   = {  size            ,  size              , -size              , -size             };
    GLfloat leftTexX[4]   = {  0.5f            ,  0.5f              ,  0.0f              ,  0.0f             };
    GLfloat leftTexY[4]   = {  0.0f            ,  0.5f              ,  0.5f              ,  0.0f             };

    skyboxModel.AddQuad( leftPosX, leftPosY, leftPosZ, leftTexX, leftTexY );

    // Right  Quad          | Right-Top-Back   | Right-Bottom-Back  | Right-Bottom-Front | Right-Top-Front   |
    GLfloat rightPosX[4]  = {  size            ,  size              ,  size              ,  size             };
    GLfloat rightPosY[4]  = {  size            , -size              , -size              ,  size             };
    GLfloat rightPosZ[4]  = { -size            , -size              ,  size              ,  size             };
    GLfloat rightTexX[4]  = {  0.5f            ,  0.5f              ,  0.0f              ,  0.0f             };
    GLfloat rightTexY[4]  = {  0.0f            ,  0.5f              ,  0.5f              ,  0.0f             };

    skyboxModel.AddQuad( rightPosX, rightPosY, rightPosZ, rightTexX, rightTexY );

    // Top  Quad            | Left-Top-Front   | Left-Top-Back      | Right-Top-Back     | Right-Top-Front   |
    GLfloat topPosX[4]    = { -size            , -size              ,  size              ,  size             };
    GLfloat topPosY[4]    = {  size            ,  size              ,  size              ,  size             };
    GLfloat topPosZ[4]    = {  size            , -size              , -size              ,  size             };
    GLfloat topTexX[4]    = {  0.5f            ,  0.5f              ,  0.0f              ,  0.0f             };
    GLfloat topTexY[4]    = {  1.0f            ,  0.5f              ,  0.5f              ,  1.0f             };

    skyboxModel.AddQuad( topPosX, topPosY, topPosZ, topTexX, topTexY );

    // Bottom Quad          | Left-Bottom-Back | Left-Bottom-Front  | Right-Bottom-Front | Right-Bottom-Back |
    GLfloat bottomPosX[4] = { -size            , -size              ,  size              ,  size             };
    GLfloat bottomPosY[4] = { -size            , -size              , -size              , -size             };
    GLfloat bottomPosZ[4] = { -size            ,  size              ,  size              , -size             };
    GLfloat bottomTexX[4] = {  1.0f            ,  1.0f              ,  0.5f              ,  0.5f             };
    GLfloat bottomTexY[4] = {  1.0f            ,  0.5f              ,  0.5f              ,  1.0f             };

    skyboxModel.AddQuad( bottomPosX, bottomPosY, bottomPosZ, bottomTexX, bottomTexY );

    // Sun                | Right-Top-Back | Right-Bottom-Back | Right-Bottom-Front | Right-Top-Front |
    GLfloat sunPosX[4]  = {  size / 2.0f   ,  size / 2.0f      ,  size / 2.0f       ,  size / 2.0f    };
    GLfloat sunPosY[4]  = {  sunSize       , -sunSize          , -sunSize           ,  sunSize        };
    GLfloat sunPosZ[4]  = { -sunSize       , -sunSize          ,  sunSize           ,  sunSize        };
    GLfloat sunTexX[4]  = {  0.0f          ,  0.0f             ,  0.5f              ,  0.5f           };
    GLfloat sunTexY[4]  = {  0.0f          ,  1.0f             ,  1.0f              ,  0.0f           };

    celestialObjectsModel.AddQuad( sunPosX, sunPosY, sunPosZ, sunTexX, sunTexY );

    // Moon               | Left-Top-Front | Left-Bottom-Front | Left-Bottom-Back   | Left-Top-Back    |
    GLfloat moonPosX[4] = { -size / 2.0f   , -size / 2.0f      , -size / 2.0f       , -size / 2.0f     };
    GLfloat moonPosY[4] = {  sunSize       , -sunSize          , -sunSize           ,  sunSize         };
    GLfloat moonPosZ[4] = {  sunSize       ,  sunSize          , -sunSize           , -sunSize         };
    GLfloat moonTexX[4] = {  0.5f          ,  0.5f             ,  1.0f              ,  1.0f            };
    GLfloat moonTexY[4] = {  0.0f          ,  1.0f             ,  1.0f              ,  0.0f            };

    celestialObjectsModel.AddQuad( moonPosX, moonPosY, moonPosZ, moonTexX, moonTexY );

    // Copy data into VBO
    celestialObjectsModel.CopyToVBO( GL_STATIC_DRAW );
    skyboxModel.CopyToVBO( GL_STATIC_DRAW );
}

Skybox::~Skybox()
{

}

/*
 * Draw the Skybox centered around the given position from
 * the perspective of the given Camera.
 */
void Skybox::Draw( const Camera& camera, WorldCoord pos, float sunAngle, glm::vec3 skyColor )
{
    static SkyboxShader shader(
        "Source/Graphics/OpenGL/Shader/Skybox/SkyboxShader.vert",
        "Source/Graphics/OpenGL/Shader/Skybox/SkyboxShader.frag"
    );

    // Bind the shader and texture
    shader.Bind();
    sf::Texture::bind( &Skybox::SkyTexture );

    glDepthMask(GL_FALSE);

    // Set the scene matrix based on the camera viewpoint
    glm::mat4 modelMatrix( 1.0f );

    modelMatrix = glm::translate( modelMatrix, pos );
    //modelMatrix = glm::scale( modelMatrix, glm::vec3(size, size, size) );

    glm::mat4 MVP = camera.GetViewProjectionMatrix() * modelMatrix;
    shader.SetMVPMatrix( MVP );
    shader.SetColour( skyColor );

    skyboxModel.Draw();

    sf::Texture::bind( &Skybox::SunMoonTexture );

    modelMatrix = glm::mat4( 1.0f );

    modelMatrix = glm::translate( modelMatrix, pos );
    modelMatrix = glm::rotate( modelMatrix, sunAngle, sunRotationAxis );
    //modelMatrix = glm::scale( modelMatrix, glm::vec3(size, size, size) );

    MVP = camera.GetViewProjectionMatrix() * modelMatrix;
    shader.SetMVPMatrix( MVP );
    shader.SetColour( glm::vec3(1, 1, 1) );

    celestialObjectsModel.Draw();

    glDepthMask(GL_TRUE);

    sf::Texture::bind( nullptr );
    shader.Unbind();
}
