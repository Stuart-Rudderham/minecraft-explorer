/*
 * This header defines a skybox that
 * encloses the entire Minecraft world
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: June 8, 2013
 */

#ifndef SKYBOX_HPP_INCLUDED
#define SKYBOX_HPP_INCLUDED

#include "World/CoordinateSystem.hpp"
#include "Graphics/Model/Model.hpp"
#include <SFML/Graphics/Texture.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>

class Camera;

class Skybox {
    public:
        Skybox();
        ~Skybox();

        void Draw( const Camera& camera, WorldCoord pos, float sunAngle, glm::vec3 skyColor );

        struct Vertex {                                                                     // A struct defining the layout of a single vertex.
            GLfloat pos[3];
            GLfloat tex[2];

            Vertex( GLfloat x, GLfloat y, GLfloat z, GLfloat u, GLfloat v )
                : pos{x, y, z}
                , tex{u, v}
            { }
        };

    private:
        static sf::Texture SkyTexture;                                                      // The texture sheet for the skybox and all objects in it
        static sf::Texture SunMoonTexture;                                                  // The texture sheet for the skybox and all objects in it

        static struct ResourceLoader {                                                      // This object will get constructed at program start and
            ResourceLoader();                                                               // it's constructor will automatically load the Skybox texture
        } Loader;

        Model<Skybox::Vertex> skyboxModel;
        Model<Skybox::Vertex> celestialObjectsModel;    // Sun, moon, stars
};

#endif // SKYBOX_HPP_INCLUDED
