/*
 * This header defines a class for loading
 * Minecraft chunk data from the filesystem.
 *
 * Author:      Stuart Rudderham
 * Created:     July 31, 2014
 * Last Edited: July 31, 2014
 */

#ifndef CHUNKLOADER_HPP_INCLUDED
#define CHUNKLOADER_HPP_INCLUDED

#include "World/CoordinateSystem.hpp"
#include <unordered_map>
#include <vector>
#include <memory>
#include <condition_variable>
#include <atomic>
#include <mutex>
#include <thread>

class Region;
class Chunk;

class ChunkLoader {
    public:
        ChunkLoader( std::string name, ChunkCoord org );
        ~ChunkLoader();

        void SetOrigin( ChunkCoord newOrigin );

        std::vector<std::unique_ptr<Chunk>> GetLoadedChunks();

        void DeferredLoad( ChunkCoord chunkCoord );

        // DELETE ME.
        void Update();

    private:
        std::string mapName;

        ChunkCoord origin;                                                      // Chunks closest to the origin are loaded first.

        std::vector<ChunkCoord> chunksToLoad;                                   // A sorted list of the Chunks to load. The input queue for the ChunkLoader.
//        std::mutex chunksToLoadMutex;

        std::vector<std::unique_ptr<Chunk>> loadedChunks;                       // A list of the Chunks loaded so far. The output queue for the ChunkLoader.
//        std::mutex loadedChunksMutex;

//        std::condition_variable loaderThreadCV;

//        std::atomic<bool> shouldExit;

//        static const int numThreads = 1;
//        std::thread loaderThreads[numThreads];

        void LoaderThreadLoop();

        std::unordered_map<RegionCoord, std::unique_ptr<Region>> loadedRegions; // A list of RegionCoords and their corresponding Region.
                                                                                // If the Region hasn't be accessed yet it will not be in the map.
                                                                                // If the Region has tried to be accessed and doesn't exist a nullptr
                                                                                // will be in the map.

        std::unique_ptr<Chunk> LoadChunk( ChunkCoord chunkCoords );             // Try and load the Chunk located at the given coordinates.
                                                                                // If the Chunk data doesn't exist (because it hasn't been
                                                                                // generated yet) then a Chunk full of Air Blocks is loaded.
};

#endif // CHUNKLOADER_HPP_INCLUDED
