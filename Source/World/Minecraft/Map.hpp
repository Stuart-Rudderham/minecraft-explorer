/*
 * This header defines an entire
 * Minecraft map loading in memory
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: July 31, 2014
 */

#ifndef MAP_HPP_INCLUDED
#define MAP_HPP_INCLUDED

#include "BasicTypes.hpp"
#include "World/CoordinateSystem.hpp"
#include "World/Minecraft/ChunkLoader.hpp"
#include <glm/glm.hpp>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <memory>
#include <fstream>

class Chunk;
class Block;
class Player;
class Camera;

class Map {
    public:
        Map( std::string name, const Player& relativeTo );                      // ctor
        ~Map();                                                                 // dtor

        void Update( float deltaT );                                            // Move the Map's center based on Player
                                                                                // location, loading/unloading Chunks as necessary

        void Draw( const Camera& camera,                                        // Draw all the Chunks that are near the
                   glm::vec3 sunlightDir,                                       // Player from the perspective of the
                   float lightIntensity ) const;                                // given Camera

        bool Colliding( WorldCoord pos ) const;

    private:
        static const int LoadDistance   = 10;                                   // How many Chunks around the Map center we have loaded
        static const int DrawDistance   = 10;                                    // How many Chunks around the Player we draw
        static const int MaxOffset      = LoadDistance - DrawDistance;
        static const int ChunkArraySize = LoadDistance * 2 + 1;                 // How big the array holding the loaded Chunks has to be

        std::string mapName;                                                    // The name of the folder where the Map's Region files are kept
        const Player& player;

        ChunkCoord mapCenter;                                                   // The center of the Map.

        mutable ChunkLoader chunkLoader;                                        // Responsible for loading Chunk data from the filesystem.

        std::unique_ptr<Chunk> chunks[ChunkArraySize][ChunkArraySize];          // The array of loaded Chunks.

        glm::ivec2 GetArrayIndex( ChunkCoord coords ) const;                    // Calculate where in the 2D array of loaded Chunks the
                                                                                // Chunk with the desired coordiates would be stored.
                                                                                // WARNING: The calculated index may be bigger than the
                                                                                // size of the array. Before actually indexing the array
                                                                                // confirm that they are valid using IsValidArrayIndex()

        bool IsValidArrayIndex( glm::ivec2 index ) const;                       // Return true if the given values are valid to index
                                                                                // the 2D array of loaded Chunks

        const Chunk* GetChunk( ChunkCoord coords,                               // Return the Chunk located at the given coordinates. If the Chunk
                               bool deferredLoad = true ) const;                // hasn't been loaded yet NULL is returned and a request to load the                                                                                // Chunk will be issued if deferredLoad == true.
};

#endif // MAP_HPP_INCLUDED
