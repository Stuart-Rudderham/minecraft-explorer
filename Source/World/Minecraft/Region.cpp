/*
 * This file implements a 32x32 area of Chunks
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 27, 2013
 */

#include "World/Minecraft/Region.hpp"
#include "World/Minecraft/Chunk.hpp"
#include "World/Minecraft/ParsingFunctions.hpp"
#include <zlib/zlib.h>

/*
 * Constructor. Reads the header from an already open Region file
 */
Region::Region( std::unique_ptr<std::ifstream> openFile, RegionCoord coords )
    : regionCoords( coords )
    , file( std::move(openFile) )
{
    // It is assumed that the passed file is valid for reading
    dbAssert( !file->fail(), "Given file not ready for reading" );

    // Read the header
    file->seekg( 0 );

    for( unsigned int i = 0; i < 1024; ++i ) {
        ReadIntegerNumber( *file, header[i] );
    }
}

/*
 * Try and load the Chunk located at the given coordinates. If the Chunk
 * has not been generated yet then an empty Chunk (i.e. full of Air blocks)
 * will be created.
 */
std::unique_ptr<Chunk> Region::LoadChunk( ChunkCoord chunkCoords )
{
    // Note: We reference coord.y because these are 2D vectors, but Chunk coords are really on the XZ plane

    // File format reference -> http://www.minecraftwiki.net/wiki/Region_file_format#Structure

    // Convert the Chunk coords from Chunk-Space (-inf to +inf) into Local-Region-Space (0 to 31)
    // If the ChunkCoords do not belong to this Region an assert will fire
    LocalChunkCoord localChunkCoords( chunkCoords, regionCoords );

    // Calculate where in the header the Chunk offset is stored
    int chunkIndex = localChunkCoords.x + (localChunkCoords.y * 32);

    // If it has a value of 0 then the Chunk hasn't been generated yet
    if( header[chunkIndex] == 0 ) {
        //dbPrintToConsole( "Chunk at ", chunkCoords, " doesn't exist, creating empty Chunk" );
        return std::unique_ptr<Chunk>( new Chunk(chunkCoords) );
    }

    // Otherwise find the Chunk's offset into the file
    Int32 chunkOffset = header[chunkIndex] >> 8;        // Need upper 3 bytes
    //Int8  sectorCount = header[chunkIndex] & 0xFF;    // Need lowest byte. Not used right now

    // Jump to where the Chunk data data is stored
    file->seekg( chunkOffset * 4 * 1024 );

    // Find how many bytes it is
    Int32 compressedDataLength;
    ReadIntegerNumber( *file, compressedDataLength );

    // The next byte states how it was compressed
    Int8 compressionType;
    ReadIntegerNumber( *file, compressionType );

    dbAssert( compressionType == 2, "Not compressed using zlib" );

    // Decompress the next compressedDataLength - 1 bytes (since we just read a
    // byte to get the compression scheme) using zlib
    compressedDataLength -= 1;

    //
    // FIXME: Can replace these vectors with std::dynarray when upgrade compiler
    //

    // zlib cannot be used with iostream, so copy the data into memory first
    std::vector<UInt8> compressed;
    compressed.reserve( compressedDataLength );
    file->read( (char*)compressed.data(), compressedDataLength );

    // Make a buffer for zlib to put the uncompressed data.
    // According to the wiki it's supposed to be "sectorCount * 4 * 1024"
    // bytes long, but that keeps failing. So we choose an arbitrary number.
    // Make it bigger if zlib starts failing
    unsigned long uncompressedDataLength = 200000;
    std::vector<unsigned char> uncompressed;
    uncompressed.reserve( uncompressedDataLength );

    int ret = uncompress( uncompressed.data(), &uncompressedDataLength, compressed.data(), compressedDataLength );
    dbAssert( ret == Z_OK, "Problem with zlib" );
    (void)ret;

    // Create a new Chunk from the uncompressed data
    //dbPrintToConsole( "Chunk at ", chunkCoords, " exists, loading now" );
    return std::unique_ptr<Chunk>( new Chunk(chunkCoords, uncompressedDataLength, uncompressed.data()) );

    // Possible idea for memory mapping the file
    /*
    fp = fopen("file.gz", "r");
    buf = mmap(0,len,PROT_READ,MAP_SHARED,fileno(fp),0);
    assert(buf);
    if(uncompress(out,&outsize,buf,len) == Z_DATA_ERROR) fprintf(stderr,"boh\n");
    */
}
