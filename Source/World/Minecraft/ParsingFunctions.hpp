/*
 * This header defines shared parsing functions
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: January 23, 2013
 */

#ifndef PARSINGFUNCTIONS_HPP_INCLUDED
#define PARSINGFUNCTIONS_HPP_INCLUDED

#include "DebugTools/Debug.hpp"
#include <fstream>
#include <iostream>

template<typename T>
void ReadIntegerNumber( std::istream& file, T& num, unsigned int readSize = sizeof(T) )
{
    UInt8 bytes[readSize];

    file.read( reinterpret_cast<char*>(&bytes), readSize );
    dbAssert( !file.fail(), "Unable to read bytes" );
    dbAssert( readSize <= sizeof(T), "Reading more than can fit in number" );

    // Need to convert from big-endian file to little-endian processing
    // This is a really slow way to do this, but whatever
    num = bytes[0];
    for( unsigned int i = 1; i < readSize; ++i ) {
        num <<= 8;
        num |= bytes[i];
    }
}

// Utility function for reading text
// Caller is responsible for freeing char*
inline
void ReadText( std::istream& file, char*& text, Int16 textLength )
{
    // Read the text. Ignores encoding, but won't display properly if it has non-ASCII characters
    text = new char[textLength + 1];

    file.read( text, textLength );
    dbAssert( !file.fail(), "Unable to read name string" );

    text[textLength] = '\0';     // read() doesn't add nul character at end, so need to add it
}

// Utility function for skipping bytes of input
inline
void Skip( std::istream& file, Int32 numBytes )
{
    file.seekg( numBytes, std::ios_base::cur );
    dbAssert( !file.fail(), "Skipping failed" );
}

#endif // PARSINGFUNCTIONS_HPP_INCLUDED
