/*
 * This file implements a class for loading
 * Minecraft chunk data from the filesystem.
 *
 * Author:      Stuart Rudderham
 * Created:     July 31, 2014
 * Last Edited: July 31, 2014
 */

#include "World/Minecraft/ChunkLoader.hpp"
#include "World/Minecraft/Region.hpp"
#include "World/Minecraft/Chunk.hpp"
#include <algorithm>

/*
 * Constructor.
 */
ChunkLoader::ChunkLoader( std::string name, ChunkCoord org )
    : mapName( name )
    , origin( org )
{
    /*
    shouldExit.store( false );
    for( int i = 0; i < numThreads; ++i )
    {
        loaderThreads[i] = std::thread( &ChunkLoader::LoaderThreadLoop, this );
    }
    */
}

/*
 * Destructor.
 */
ChunkLoader::~ChunkLoader()
{
    // Since we're using forward declared types with std::unique_ptr we have explcitly declare
    // the ChunkLoader destructor. Gotta love C++ ...
    // See -> http://stackoverflow.com/questions/9020372/how-do-i-use-unique-ptr-for-pimpl
    // And -> http://stackoverflow.com/questions/5460186/preventing-header-explosion-in-c-or-c0x

    /*
    shouldExit.store( true );
    loaderThreadCV.notify_all();

    for( int i = 0; i < numThreads; ++i )
    {
        //loaderThreads[i].detach();
        loaderThreads[i].join();
    }
    */
}

void ChunkLoader::SetOrigin( ChunkCoord newOrigin )
{
    //std::lock_guard<std::mutex> lk( chunksToLoadMutex );

    origin = newOrigin;

    // If Chunks are equidistant then we might as well get them in the order requested.
    std::stable_sort (
        chunksToLoad.begin(),
        chunksToLoad.end(),
        [&]( const ChunkCoord& lhs, const ChunkCoord& rhs ) {
            glm::ivec2 lhsDisplacement = lhs - origin;
            glm::ivec2 rhsDisplacement = rhs - origin;

            return glm::dot(lhsDisplacement, lhsDisplacement) <
                   glm::dot(rhsDisplacement, rhsDisplacement);
        }
    );
}

void ChunkLoader::DeferredLoad( ChunkCoord chunkCoord )
{
    //std::lock_guard<std::mutex> lk( chunksToLoadMutex );

    // Currently O(n) when could be O(log n) since array is sorted.
    if( std::find(chunksToLoad.begin(), chunksToLoad.end(), chunkCoord) == chunksToLoad.end() ) {
        chunksToLoad.push_back(chunkCoord);
        //loaderThreadCV.notify_one();
    }
}

void ChunkLoader::Update()
{
    // Load some previously requested Chunks and insert them into the array
    const int MaxChunksToLoadPerFrame = 1;

    for( int i = 0; i < MaxChunksToLoadPerFrame && chunksToLoad.empty() == false; /*Increment will happen when we actually load a Chunk*/ )
    {
        // Remove the request from the list
        ChunkCoord requestedChunkCoords = *chunksToLoad.begin();
        chunksToLoad.erase( chunksToLoad.begin() );

        // Otherwise load the Chunk
        loadedChunks.push_back( LoadChunk(requestedChunkCoords) );
        ++i;
    }
}

void ChunkLoader::LoaderThreadLoop()
{
    /*
    // Loop forever until told to exit.
    while( shouldExit.load() == false )
    {
        // Wait until there are things to do (either load Chunks or exit).
        std::unique_lock<std::mutex> chunksToLoadLock( chunksToLoadMutex );

        loaderThreadCV.wait( chunksToLoadLock,
            [this]() {
                return chunksToLoad.empty() == false || shouldExit.load() == true;
            }
        );

        // Clear out the list of Chunks to load.
        while( chunksToLoad.empty() == false )
        {
            auto requestedChunkCoords = chunksToLoad.back();
            chunksToLoad.pop_back();

            // Release the lock while loading the Chunk since that will take a while.
            chunksToLoadLock.unlock();
            std::unique_ptr<Chunk> newChunk = LoadChunk(requestedChunkCoords);

            // Add the loaded Chunk to the list of loaded Chunks waiting to be processed by the main thread.
            std::unique_lock<std::mutex> loadedChunksLock( loadedChunksMutex );
            loadedChunks.push_back( std::move(newChunk) );
            loadedChunksLock.unlock();

            // Reacquire the lock since we need to hold it when checking the loop condition.
            chunksToLoadLock.lock();
        }

        // Go back to sleep.
    }
    */
}


std::vector<std::unique_ptr<Chunk>> ChunkLoader::GetLoadedChunks()
{
    //std::lock_guard<std::mutex> lk( loadedChunksMutex );

    std::vector<std::unique_ptr<Chunk>> ret;

    std::swap( loadedChunks, ret );

    return std::move( ret );
}

/*
 * Load the Chunk located at the given coordinates.
 * If the Chunk data doesn't exist (because it hasn't been
 * generated yet) then a Chunk full of Air Blocks is loaded.
 */
std::unique_ptr<Chunk> ChunkLoader::LoadChunk( ChunkCoord chunkCoords )
{
    // Find what Region the Chunk belongs to
    RegionCoord regionCoords = chunkCoords.ToRegion();
    Region* region = nullptr;

    // If we have tried to access the Region before then just get it
    if( loadedRegions.count(regionCoords) > 0 ) {
        region = loadedRegions.find( regionCoords )->second.get();
    }
    // If we haven't tried to access the Region before
    else {
        // Try and open the Region's data file
        std::stringstream regionFileName;
        regionFileName << "Content/Maps/" << mapName << "/r." << regionCoords.x << "." << regionCoords.y << ".mca";
        std::unique_ptr<std::ifstream> file( new std::ifstream(regionFileName.str(), std::ios_base::in | std::ios_base::binary) );

        // If the file exists then load the Region
        if( !file->fail() ) {
            std::unique_ptr<Region> newRegion( new Region(std::move(file), regionCoords) );
            region = newRegion.get();

            loadedRegions[regionCoords] = std::move(newRegion);
        }
        // Otherwise the Region doesn't exist
        else {
            loadedRegions[regionCoords] = nullptr;
        }
    }

    // If the Region exists
    if( region != nullptr ) {
        //dbPrintToConsole( "Region ", regionCoords, " exists, loading Chunk at ", chunkCoords );
        return region->LoadChunk( chunkCoords );
    }
    // If the Region doesn't exist
    else {
        //dbPrintToConsole( "Region ", regionCoords, " doesn't exist, creating empty Chunk at ", chunkCoords );
        return std::unique_ptr<Chunk>( new Chunk(chunkCoords) );
    }
}
