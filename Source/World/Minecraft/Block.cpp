/*
 * This file implements a single block
 * in a chunk.
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: January 23, 2013
 */

#include "World/Minecraft/Block.hpp"
#include "Graphics/Model/Model.hpp"
#include "DebugTools/Debug.hpp"
#include <SFML/Window/Context.hpp>

#ifndef M_SQRT2
    #define M_SQRT2    1.41421356237309504880168872420969808
#endif


// Static variables
sf::Texture           Block::Texture;
GLfloat               Block::Size    = 1.0f;
GLfloat               Block::TexSize = 1.0f / 16.0f;
Block::BlockData      Block::ConstData[Block::NumTypes];
Block::ResourceLoader Block::Loader;

Block::ResourceLoader::ResourceLoader()
{
    // Make sure we have an OpenGL context
    sf::Context context;

    // Load the texture sheet
    dbErrorCheck( Block::Texture.loadFromFile("Content/Images/terrain.png"), "Unable to load Block texture" );
    dbAssert( Block::Texture.getSize().x == Block::Texture.getSize().y, "Block texture not square" );
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // Setup default
    for( unsigned int i = 1; i < Block::NumTypes; ++i ) {
        //float tX = i / 16;  // random texture based on block ID
        //float tY = i % 16;

        float tX = 10;  // bright purple texture
        float tY = 1;

        Block::ConstData[i] = { "Unset", {tX, tY}, {tX, tY}, Visibility::Opaque, Structure::Cube };
    }

    // Specific blocks
    Block::ConstData[0]   = { "Air"             , {10, 1 }, {10, 1 }, Visibility::Transparent, Structure::Cube  };
    Block::ConstData[1]   = { "Stone"           , {1,  0 }, {1,  0 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[2]   = { "Grass Block"     , {3,  0 }, {8,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[3]   = { "Dirt"            , {2,  0 }, {2,  0 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[4]   = { "Cobblestone"     , {0,  1 }, {0,  1 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[5]   = { "Wood Planks"     , {4,  0 }, {4,  0 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[7]   = { "Bedrock"         , {1,  1 }, {1,  1 }, Visibility::Opaque     , Structure::Cube  };
    //Block::ConstData[9]   = { "Water"           , {13, 12}, {13, 12}, Visibility::Opaque     , Structure::Cube  };        // Solid Water
    Block::ConstData[9]   = { "Water"           , {13, 12}, {13, 12}, Visibility::Translucent, Structure::Cube  };        // Translucent Water
    Block::ConstData[11]  = { "Lava"            , {13, 14}, {13, 14}, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[12]  = { "Sand"            , {2,  1 }, {2,  1 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[13]  = { "Gravel"          , {0,  0 }, {0,  0 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[14]  = { "Gold Ore"        , {0,  2 }, {0,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[15]  = { "Iron Ore"        , {1,  2 }, {1,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[16]  = { "Coal Ore"        , {2,  2 }, {2,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[17]  = { "Wood"            , {4,  1 }, {4,  1 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[18]  = { "Leaves"          , {4,  3 }, {4,  3 }, Visibility::Transparent, Structure::Cube  };        // Transparent Leaves
    //Block::ConstData[18]  = { "Leaves"          , {5,  3 }, {5,  3 }, Visibility::Opaque     , Structure::Cube  };        // Solid Leaves
    Block::ConstData[21]  = { "Lapis Lazuli Ore", {0,  10}, {0,  10}, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[24]  = { "Sandstone"       , {0,  11}, {0,  11}, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[30]  = { "Cobweb"          , {11, 0 }, {11, 0 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[31]  = { "Grass"           , {7,  2 }, {7,  2 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[32]  = { "Dead Bush"       , {7,  3 }, {7,  3 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[37]  = { "Dandelion"       , {13, 0 }, {13, 0 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[38]  = { "Rose"            , {12, 0 }, {12, 0 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[39]  = { "Brown Mushroom"  , {13, 1 }, {13, 1 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[40]  = { "Red Mushroom"    , {12, 1 }, {12, 1 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[48]  = { "Moss Cobblestone", {4,  2 }, {4,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[49]  = { "Obsidian"        , {5,  2 }, {5,  2 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[51]  = { "Fire"            , {15, 1 }, {15, 1 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[56]  = { "Diamond Ore"     , {2,  3 }, {2,  3 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[73]  = { "Redstone Ore"    , {3,  3 }, {3,  3 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[78]  = { "Snow"            , {2,  4 }, {2,  4 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[79]  = { "Ice"             , {3,  4 }, {3,  4 }, Visibility::Translucent, Structure::Cube  };
    Block::ConstData[81]  = { "Cactus"          , {6,  4 }, {5,  4 }, Visibility::Transparent, Structure::Cube  };
    Block::ConstData[82]  = { "Clay"            , {8,  4 }, {8,  4 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[83]  = { "Sugar Cane"      , {9,  4 }, {9,  4 }, Visibility::Transparent, Structure::Cross };
    Block::ConstData[86]  = { "Pumpkin"         , {6,  7 }, {6,  6 }, Visibility::Opaque     , Structure::Cube  };
    Block::ConstData[106] = { "Vines"           , {15, 8 }, {15, 8 }, Visibility::Transparent, Structure::Cube  };
    Block::ConstData[111] = { "Lily Pad"        , {12, 4 }, {12, 4 }, Visibility::Transparent, Structure::Flat  };

    //Block::ConstData[85]  = Block::ConstData[5];                               // fence            -> wood plank
    Block::ConstData[8]   = Block::ConstData[9];                               // stationary water -> water
    Block::ConstData[10]  = Block::ConstData[11];                              // stationary lava  -> lava
    //Block::ConstData[127] = Block::ConstData[18];                              // cocoa beans      -> leaves

    // Normalize all texture coords
    for( unsigned int i = 1; i < Block::NumTypes; ++i ) {
        Block::ConstData[i].sideTex[0]      *= TexSize;
        Block::ConstData[i].sideTex[1]      *= TexSize;

        Block::ConstData[i].topBottomTex[0] *= TexSize;
        Block::ConstData[i].topBottomTex[1] *= TexSize;

        // Half pixel correction
        //Block::ConstData[i].sideTex[0]      += 0.5f / 256.0f;
        //Block::ConstData[i].sideTex[1]      += 0.5f / 256.0f;

        //Block::ConstData[i].topBottomTex[0] += 0.5f / 256.0f;
        //Block::ConstData[i].topBottomTex[1] += 0.5f / 256.0f;
    }
    // Half pixel correction
    //TexSize -= (0.5f / 128.0f);

    static_assert( sizeof(Block) == 1, "Block has increased in size!" );
    dbPrintToConsole( "sizeof(Block) = ", sizeof(Block) );
    dbPrintToConsole( "Texture pack is ", Block::Texture.getSize().x, "x", Block::Texture.getSize().y );
    dbPrintToConsole( "A single texture is ", Block::Texture.getSize().x / 16.0f, "x", Block::Texture.getSize().x / 16.0f );
}

template<>
void Model<Block::Vertex>::DefineVAOLayout()
{
    // For explanation of use of sizeof() and offsetof() see -> http://www.opengl.org/wiki/Vertex_Array_Objects#Vertex_buffer_offset_and_stride

    // Vertex position data is stored at location 0
    SetAttribute(
        0,                                                          // Location
        3,                                                          // Size
        GL_FLOAT,                                                   // Type
        GL_FALSE,                                                   // Normalized?
        sizeof( Block::Vertex ),                                    // Stride
        reinterpret_cast<void*>( offsetof(Block::Vertex, pos) )     // Array buffer offset
    );

    // Texture coordinate data is stored at location 1
    SetAttribute(
        1,                                                          // Location
        2,                                                          // Size
        GL_FLOAT,                                                   // Type
        GL_FALSE,                                                   // Normalized?
        sizeof( Block::Vertex ),                                    // Stride
        reinterpret_cast<void*>( offsetof(Block::Vertex, tex) )     // Array buffer offset
    );

    // Normal data is stored at location 2
    SetAttribute(
        2,                                                          // Location
        3,                                                          // Size
        GL_FLOAT,                                                   // Type
        GL_FALSE,                                                   // Normalized?
        sizeof( Block::Vertex ),                                    // Stride
        reinterpret_cast<void*>( offsetof(Block::Vertex, norm) )    // Array buffer offset
    );

    //Unbind();
}

Block::Block()
    : type( 0 )
{

}

void Block::AddAllFaces( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    if( IsCubeShaped() ) {
        // 6 faces
        //     6 * 6 3D vertex positions    (108 floats, 432 bytes)
        //     6 * 6 2D texture coordinates (72 floats , 288 bytes)
        //     ----------------------------------------------
        //     Total: 180 floats, 720 bytes

        AddFrontFace ( pos, model );
        AddBackFace  ( pos, model );
        AddLeftFace  ( pos, model );
        AddRightFace ( pos, model );
        AddTopFace   ( pos, model );
        AddBottomFace( pos, model );
    } else if( IsCrossShaped() ) {
        // 4 faces
        //     4 * 6 3D vertex positions    (72 floats, 288 bytes)
        //     4 * 6 2D texture coordinates (48 floats, 192 bytes)
        //     ----------------------------------------------
        //     Total: 120 floats, 480 bytes

        AddCCWDiagonalFaces( pos, model );
        AddCWDiagonalFaces ( pos, model );
    } else /*if( IsFlat() ) */{
        // 1 face
        //     1 * 6 3D vertex positions    (18 floats, 72 bytes)
        //     1 * 6 2D texture coordinates (12 floats, 48 bytes)
        //     ----------------------------------------------
        //     Total: 30 floats, 120 bytes

        AddHorizontalFlatFace( pos, model );
    }
}

void Block::AddFrontFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                | Left-Top-Front        | Left-Bottom-Front     | Right-Bottom-Front    | Right-Top-Front        |
    GLfloat posX[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ[4] = { pos.z + Block::Size   , pos.z + Block::Size   , pos.z + Block::Size   , pos.z + Block::Size    };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norY[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ[4] = { +1.0f                 , +1.0f                 , +1.0f                 , +1.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddBackFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                | Right-Top-Back        | Right-Bottom-Back     | Left-Bottom-Back      | Left-Top-Back          |
    GLfloat posX[4] = { pos.x + Block::Size   , pos.x + Block::Size   , pos.x                 , pos.x                  };
    GLfloat posY[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ[4] = { pos.z                 , pos.z                 , pos.z                 , pos.z                  };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norY[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ[4] = { -1.0f                 , -1.0f                 , -1.0f                 , -1.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddLeftFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                | Left-Top-Back         | Left-Bottom-Back      | Left-Bottom-Front     | Left-Top-Front         |
    GLfloat posX[4] = { pos.x                 , pos.x                 , pos.x                 , pos.x                  };
    GLfloat posY[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ[4] = { pos.z                 , pos.z                 , pos.z + Block::Size   , pos.z + Block::Size    };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = { -1.0f                 , -1.0f                 , -1.0f                 , -1.0f                  };
    GLfloat norY[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddRightFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                | Right-Top-Front       | Right-Bottom-Front    | Right-Bottom-Back     | Right-Top-Back         |
    GLfloat posX[4] = { pos.x + Block::Size   , pos.x + Block::Size   , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ[4] = { pos.z + Block::Size   , pos.z + Block::Size   , pos.z                 , pos.z                  };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = { +1.0f                 , +1.0f                 , +1.0f                 , +1.0f                  };
    GLfloat norY[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddTopFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].topBottomTex[0], Block::ConstData[type].topBottomTex[1] );

    //                | Left-Top-Back         | Left-Top-Front        | Right-Top-Front       | Right-Top-Back         |
    GLfloat posX[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY[4] = { pos.y + Block::Size   , pos.y + Block::Size   , pos.y + Block::Size   , pos.y + Block::Size    };
    GLfloat posZ[4] = { pos.z                 , pos.z + Block::Size   , pos.z + Block::Size   , pos.z                  };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norY[4] = { +1.0f                 , +1.0f                 , +1.0f                 , +1.0f                  };
    GLfloat norZ[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddBottomFace( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].topBottomTex[0], Block::ConstData[type].topBottomTex[1] );

    //                | Left-Bottom-Front     | Left-Bottom-Back      | Right-Bottom-Back     | Right-Bottom-Front     |
    GLfloat posX[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY[4] = { pos.y                 , pos.y                 , pos.y                 , pos.y                  };
    GLfloat posZ[4] = { pos.z + Block::Size   , pos.z                 , pos.z                 , pos.z + Block::Size    };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norY[4] = { -1.0f                 , -1.0f                 , -1.0f                 , -1.0f                  };
    GLfloat norZ[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}

void Block::AddCCWDiagonalFaces( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 4 triangles
    //     12 3D vertex positions    (36 floats, 144 bytes)
    //     12 2D texture coordinates (24 floats, 96 bytes)
    //     ----------------------------------------------
    //     Total: 60 floats, 240 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                 | Left-Top-Front        | Left-Bottom-Front     | Right-Bottom-Back     | Right-Top-Back         |
    GLfloat posX1[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY1[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ1[4] = { pos.z + Block::Size   , pos.z + Block::Size   , pos.z                 , pos.z                  };

    GLfloat texX1[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY1[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX1[4] = {  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f        };
    GLfloat norY1[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ1[4] = {  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f        };

    model.AddQuad( posX1, posY1, posZ1, texX1, texY1, norX1, norY1, norZ1 );

    //                 | Left-Top-Back         | Left-Bottom-Back      | Right-Bottom-Front    | Right-Top-Front        |
    GLfloat posX2[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY2[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ2[4] = { pos.z                 , pos.z                 , pos.z + Block::Size   , pos.z + Block::Size    };

    GLfloat texX2[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY2[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX2[4] = { -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f        };
    GLfloat norY2[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ2[4] = {  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f        };

    model.AddQuad( posX2, posY2, posZ2, texX2, texY2, norX2, norY2, norZ2 );
}

void Block::AddCWDiagonalFaces( WorldCoord pos, Model<Block::Vertex>& model ) const
{
    // 4 triangles
    //     12 3D vertex positions    (36 floats, 144 bytes)
    //     12 2D texture coordinates (24 floats, 96 bytes)
    //     ----------------------------------------------
    //     Total: 60 floats, 240 bytes

    glm::vec2 tex( Block::ConstData[type].sideTex[0], Block::ConstData[type].sideTex[1] );

    //                 | Right-Top-Back        | Right-Bottom-Back     | Left-Bottom-Front     | Left-Top-Front         |
    GLfloat posX1[4] = { pos.x + Block::Size   , pos.x + Block::Size   , pos.x                 , pos.x                  };
    GLfloat posY1[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ1[4] = { pos.z                 , pos.z                 , pos.z + Block::Size   , pos.z + Block::Size    };

    GLfloat texX1[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY1[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX1[4] = { -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f        };
    GLfloat norY1[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ1[4] = { -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f        };

    model.AddQuad( posX1, posY1, posZ1, texX1, texY1, norX1, norY1, norZ1 );

    //                 | Right-Top-Front       | Right-Bottom-Front    | Left-Bottom-Back      | Left-Top-Back          |
    GLfloat posX2[4] = { pos.x + Block::Size   , pos.x + Block::Size   , pos.x                 , pos.x                  };
    GLfloat posY2[4] = { pos.y + Block::Size   , pos.y                 , pos.y                 , pos.y + Block::Size    };
    GLfloat posZ2[4] = { pos.z + Block::Size   , pos.z + Block::Size   , pos.z                 , pos.z                  };

    GLfloat texX2[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY2[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX2[4] = {  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f       ,  M_SQRT2 / 2.0f        };
    GLfloat norY2[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norZ2[4] = { -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f       , -M_SQRT2 / 2.0f        };

    model.AddQuad( posX2, posY2, posZ2, texX2, texY2, norX2, norY2, norZ2 );
}

void Block::AddHorizontalFlatFace( WorldCoord pos, Model<Block::Vertex>& model ) const {
    // 2 triangles
    //     6 3D vertex positions    (18 floats, 72 bytes)
    //     6 2D texture coordinates (12 floats, 48 bytes)
    //     ----------------------------------------------
    //     Total: 30 floats, 120 bytes

    glm::vec2 tex( Block::ConstData[type].topBottomTex[0], Block::ConstData[type].topBottomTex[1] );

    //                | Left-Bottom-Back      | Left-Bottom-Front     | Right-Bottom-Front    | Right-Bottom-Back      |
    GLfloat posX[4] = { pos.x                 , pos.x                 , pos.x + Block::Size   , pos.x + Block::Size    };
    GLfloat posY[4] = { pos.y                 , pos.y                 , pos.y                 , pos.y                  };
    GLfloat posZ[4] = { pos.z                 , pos.z + Block::Size   , pos.z + Block::Size   , pos.z                  };

    GLfloat texX[4] = { tex.x                 , tex.x                 , tex.x + Block::TexSize, tex.x + Block::TexSize };
    GLfloat texY[4] = { tex.y                 , tex.y + Block::TexSize, tex.y + Block::TexSize, tex.y                  };

    GLfloat norX[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };
    GLfloat norY[4] = { +1.0f                 , +1.0f                 , +1.0f                 , +1.0f                  };
    GLfloat norZ[4] = {  0.0f                 ,  0.0f                 ,  0.0f                 ,  0.0f                  };

    model.AddQuad( posX, posY, posZ, texX, texY, norX, norY, norZ );
}
