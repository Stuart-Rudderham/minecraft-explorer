/*
 * This header defines a 32x32 area of Chunks
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 27, 2013
 */

#ifndef REGION_HPP_INCLUDED
#define REGION_HPP_INCLUDED

#include "BasicTypes.hpp"
#include "World/CoordinateSystem.hpp"
#include <glm/glm.hpp>
#include <fstream>
#include <memory>
#include <array>

// Forward declarations
class Chunk;

class Region {
    public:
        Region( std::unique_ptr<std::ifstream> openFile, RegionCoord coords );      // Ctor. Taken in the already open file that holds
                                                                                    // all the data for the Chunks located within the
                                                                                    // Region as well as the coordinates of the Region
                                                                                    // in Region-Space.

        std::unique_ptr<Chunk> LoadChunk( ChunkCoord chunkCoords );                 // Given the coordinates of a Chunk in Chunk-Space
                                                                                    // load the Chunk from the data file and return a
                                                                                    // reference to it. If the Chunk data does not exist
                                                                                    // (because the Chunk hasn't been generated yet) then
                                                                                    // a Chunk full of Air Blocks will be returned. If
                                                                                    // the given Chunk does not belong to the Region an
                                                                                    // assert will trigger.
    private:
        RegionCoord regionCoords;                                                   // The coordinated of the Region in Region-Space
        std::unique_ptr<std::ifstream> file;                                        // The file that holds all the Chunk data
        std::array<Int32, 1024> header;                                             // The header of the data file
};

#endif // REGION_HPP_INCLUDED
