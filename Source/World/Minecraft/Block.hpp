/*
 * This header defines a single block
 * in a Chunk.
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: May 19, 2013
 */

#ifndef BLOCK_HPP_INCLUDED
#define BLOCK_HPP_INCLUDED

#include "BasicTypes.hpp"
#include "World/CoordinateSystem.hpp"
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <glm/glm.hpp>
#include <vector>
#include <string>

// Forward declarations
template<typename VertexType> class Model;

class Block {
    public:
        enum class Visibility {
            Opaque,                 // - The block is completely enclosed and has a solid texture.
                                    // - It can be drawn with face culling enabled and doesn't need alpha blending
                                    // - If a block is completely surrounded by COMPLETELY_SOLID blocks it doesn't
                                    // need to be drawn, as there is no possible way it can be seen
                                    // - An example would be Cobblestone

            Transparent,            // - The block *may* not be completely enclosed or it has transparent parts in its texture.
                                    // - Its texture has no alpha, each pixel is either completely solid or completely transparent
                                    // - It does not require alpha blending, but cannot be drawn with face culling enabled.
                                    // - However, actual Minecraft *does* draw these blocks with cull enabled, so it's not a huge deal
                                    // - If a block is completely surrounded by TRANSPARENT_SECTIONS blocks it should still be drawn
                                    // - Two examples are Glass and Dandelion blocks

            Translucent             // - This block's texture has an alpha component
                                    // - It requires face culling disabled, alpha blending enabled, as to be drawn in the correct order
                                    // - If a block is completely surrounded by NEEDS_ALPHA_BLENDING blocks it should still be drawn
                                    // - An example would be Water
        };

        enum class Structure {
            Cube,                   // The block is a solid cube. For example, Cobblestone
            Cross,                  // The block is made up of two crossing quads. For example, Dandelions or Grass
            Flat                    // The block is made up of a single, horizontal quad. For example, lily pads, and *not* vines
        };

        struct BlockData {
            std::string typeName;                                                           // The name of the type of block (e.g. "Cobblestone")
            GLfloat sideTex[2];                                                             // The texture coordinates for the sides of the Block
            GLfloat topBottomTex[2];                                                        // The texture coordinates for the top and bottom of the Block
            Visibility visibility;                                                          // The visibility type of the block
            Structure structure;                                                            // The structure type of the block
        };

        struct Vertex {                                                                     // A struct defining the layout of a single vertex.
            GLfloat pos[3];
            GLfloat tex[2];
            GLfloat norm[3];

            Vertex( GLfloat pX, GLfloat pY, GLfloat pZ, GLfloat u, GLfloat v, GLfloat nX, GLfloat nY, GLfloat nZ )
                : pos {pX, pY, pZ}
                , tex {u , v}
                , norm{nX, nY, nZ}
            { }
        };

        typedef Model<Vertex> BModel;

        Block();                                                                            // Ctor. Sets the block type to "air"

        void AddAllFaces  ( WorldCoord pos, Model<Block::Vertex>& model ) const;            // Add all sides of the Block to the given buffers.
                                                                                            //     The bottom-left-back corner is at (0, 0, 0)
                                                                                            //     The top-right-front  corner is at (1, 1, 1)
                                                                                            // Very helpful link -> http://stackoverflow.com/questions/8142388/in-what-order-should-i-send-my-vertices-to-opengl-for-culling

        void AddFrontFace ( WorldCoord pos, Model<Block::Vertex>& model ) const;            // Add only a single side of the Block to the given buffers
        void AddBackFace  ( WorldCoord pos, Model<Block::Vertex>& model ) const;
        void AddLeftFace  ( WorldCoord pos, Model<Block::Vertex>& model ) const;
        void AddRightFace ( WorldCoord pos, Model<Block::Vertex>& model ) const;
        void AddTopFace   ( WorldCoord pos, Model<Block::Vertex>& model ) const;
        void AddBottomFace( WorldCoord pos, Model<Block::Vertex>& model ) const;

        void AddCCWDiagonalFaces( WorldCoord pos, Model<Block::Vertex>& model ) const;
        void AddCWDiagonalFaces ( WorldCoord pos, Model<Block::Vertex>& model ) const;

        void AddHorizontalFlatFace( WorldCoord pos, Model<Block::Vertex>& model ) const;

        inline bool ShouldDraw() const {
            return type > 0;    // anything not air
        }

        inline bool CompletelySolid() const {
            return Block::ConstData[type].visibility == Visibility::Opaque;
        }

        inline bool RequiresAlphaBlending() const {
            return Block::ConstData[type].visibility == Visibility::Translucent;
        }

        inline bool IsCubeShaped() const {
            return Block::ConstData[type].structure == Structure::Cube;
        }

        inline bool IsCrossShaped() const {
            return Block::ConstData[type].structure == Structure::Cross;
        }

        inline bool IsFlat() const {
            return Block::ConstData[type].structure == Structure::Flat;
        }

        inline void SetType( Int8 newType ) {
            type = newType;
        }

        static inline void BindTexture() {
            sf::Texture::bind( &Block::Texture );
        }

    private:
        static sf::Texture Texture;                                                         // The common texture sheet for all Blocks

        static GLfloat Size;                                                                // The dimension of the Block
        static GLfloat TexSize;                                                             // The size of a single texture from the texture sheet, in normalized coordinates (i.e. [0, 1])

        static const unsigned int NumTypes = 128;                                           // The number of different block types
        static BlockData ConstData[NumTypes];                                               // Const data for each block type

        static struct ResourceLoader {                                                      // This object will get constructed at program start and
            ResourceLoader();                                                               // it's constructor will automatically load the Block texture
        } Loader;                                                                           // and initialize all the const data

        Int8 type;
};

#endif // BLOCK_HPP_INCLUDED
