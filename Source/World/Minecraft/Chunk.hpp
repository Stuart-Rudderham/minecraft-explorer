/*
 * This header defines a single 16
 * block wide, 16 block long, and
 * 256 block deep chunk
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: August 5, 2013
 */

#ifndef CHUNK_HPP_INCLUDED
#define CHUNK_HPP_INCLUDED

#include "World/CoordinateSystem.hpp"
#include "World/Minecraft/Block.hpp"
#include "Graphics/Model/Model.hpp"
#include "DebugTools/Debug.hpp"
#include <glm/glm.hpp>
#include <map>
#include <vector>

class NBTParser;

class Chunk {
    public:
        static const int SizeX     = 16;                                    // The dimensions of the Chunk
        static const int SizeY     = 256;
        static const int SizeZ     = 16;
        static const int MaxBlocks = SizeX * SizeY * SizeZ;                 // The total number of Block stored in the Chunk

        Chunk( ChunkCoord coords,                                           // Ctor. Takes in a blob of binary data that needs to be parsed to create the Chunk
               unsigned long length = 0,
               unsigned char* data = nullptr );
        ~Chunk();// = default;

        void DrawOpaqueBlocks()      const;                                 // Draw all Blocks that do not require alpha-blending
        void DrawTranslucentBlocks() const;                                 // Draw all Blocks that do require alpha-blending

        inline ChunkCoord GetLocalPosition() const {                        // Return the position of the Chunk in Chunk-Spacelocal coordinates of the Chunk (i.e. the Chunks
            return ChunkCoord( xPos, zPos );
        }

        inline WorldCoord GetGlobalPosition() const {                       // Return the global coordinates of the Chunk
            return WorldCoord( xPos * 16, 0, zPos * 16 );
        }

        const glm::mat4 GetModelMatrix() const;

        void CreateModel();

        inline int GetNumVertices() const {
            return opaqueBlockModel.GetNumVertices() + translucentBlockModel.GetNumVertices();
        }

        // FIXME: This is a really hacky way to get Chunk fade-in
        // Refactor to make cleaner. Should record creation time
        // and not how long it has been created for
        inline void IncreaseTimeLoaded( float deltaT ) const {
            timeLoaded += deltaT;
        }

        inline float GetTimeLoaded() const {
            return timeLoaded;
        }

        inline float DistanceFrom( WorldCoord p ) const {
            WorldCoord worldCoords = GetGlobalPosition();

            WorldCoord closestPoint = WorldCoord( glm::clamp( p.x, worldCoords.x, worldCoords.x + 16.0f  ),
                                                  glm::clamp( p.y, worldCoords.y, worldCoords.y + 256.0f ),
                                                  glm::clamp( p.z, worldCoords.z, worldCoords.z + 16.0f  ) );

            glm::vec3 distance2 = p - closestPoint;
            return glm::dot(distance2, distance2);
        }

        inline const Block& GetBlock( int x, int y, int z ) const {
            return blockArr[ GetArrayIndex(x, y, z) ];
        }

        inline const Block& GetBlock( LocalBlockCoord coord ) const {
            return GetBlock(coord.x, coord.y, coord.z);
        }

        // This setter is used by NBT parser when reading the Chunk data
        void SetBlockTypes( Int8* data, Int32 length, Int8 section );       // Copy the data into the current section of
                                                                            // the Chunk that is being read

    private:
        Int32 xPos, zPos;                                                   // The coords of the Chunk in Chunk-space
                                                                            //     xPos is in the range [-inf, +inf]
                                                                            //     zPos is in the range [-inf, +inf]

        Block blockArr[MaxBlocks];                                          // The array of Blocks, in a YZX order.
                                                                            // It is a single-dimension array
                                                                            // because they are much simpler to
                                                                            // deal with and calculating indices
                                                                            // manually is easy.

        Model<Block::Vertex> opaqueBlockModel;                              // The Model for all Blocks that don't require alpha-blending to be drawn
        Model<Block::Vertex> translucentBlockModel;                         // The Model for all Blocks that do require alpha-blending to be drawn

        bool hasData;

        mutable float timeLoaded;                                           // FIXME: This should record its creation time, not how long it has been loaded for



        inline int GetArrayIndex(int x, int y, int z) const {               // Do the math to index the flat array of
            dbAssert( y >= 0 && y < SizeY, "Invalid Y index" );             // Blocks as a 3D array
            dbAssert( z >= 0 && z < SizeZ, "Invalid Z index" );
            dbAssert( x >= 0 && x < SizeX, "Invalid X index" );

            return y * SizeZ * SizeX + z * SizeX + x;

        }


};

#endif // CHUNK_HPP_INCLUDED
