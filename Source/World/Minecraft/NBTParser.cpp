/*
 * This file implements a parser for
 * a file in the Named-Binary-Tag (NBT)
 * format
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: August 27, 2013
 */

#include "World/Minecraft/NBTParser.hpp"
#include "World/Minecraft/Chunk.hpp"
#include "World/Minecraft/ParsingFunctions.hpp"
#include "DebugTools/Debug.hpp"
#include <iostream>
#include <cstring>

/*
 * All the possible NBT IDs
 */
enum NamedBinaryTag : Int8
{
    TAG_End,
    TAG_Byte,
    TAG_Short,
    TAG_Int,
    TAG_Long,
    TAG_Float,
    TAG_Double,
    TAG_Byte_Array,
    TAG_String,
    TAG_List,
    TAG_Compound,
    TAG_Int_Array
};

/*
 * The names of tags that we care about
 */
const char* TagXPos     = "xPos";
const char* TagZPos     = "zPos";
const char* TagY        = "Y";
const char* TagSections = "Sections";
const char* TagBlocks   = "Blocks";
const char* TagData     = "Data";

/*
 * Constructor. Sets the state to an invalid value
 */
NBTParser::NBTParser( Chunk& c )
    : chunk( c )
    , currentSection( -1 )
{

}

/*
 * Initialize the Chunk by parsing the given binary blob of NBT-formatted data
 */
void NBTParser::Parse( unsigned char* data, unsigned long length )
{
    // We want to be able to use the iostream interface, so
    // have an std::stringstream point at the uncompressed data buffer
    // Implementation defined behaviour. Works on GCC, not Visual Studio
    std::istringstream in;
    in.rdbuf()->pubsetbuf( reinterpret_cast<char*>(data), length );

    // Root tag has to be a TAG_Compound
    dbAssert( PeekNextTagType(in) == TAG_Compound, "Invalid, first tag isn't TAG_Compound" );

    // Read the root tag and all sub-tags
    ParseTAG_Compound( in, /*shouldIgnore*/false, /*hasName*/true, /*hasType*/true );

    // Should read entire file while handling root tag
    dbAssert( !in.eof(), "Invalid stream, has data not enclosed in root tag" );

    // We're done editing sections now
    currentSection = -1;
}

/*
 * All NBT tags (unless they are in a list) have three things:
 *     1) A type identifier
 *     2) A name
 *     3) A payload
 *
 * The only exception to this are tags that are part of a TAG_List. These
 * tags only have a payload.
 *
 * This macro inserts code to read the tag type identifier as well as
 * the tag name (if they exist). It's *super* hacky, since it creates
 * variables as well as assumes that variables with certain names exist
 * in the function the macro is placed in.
 *
 */
#define COMMON_HEADER(Type)                                             \
    /* Declare variables needed to handle all tags */                   \
    Int8 tagType = -1;                                                  \
    char* tagName = NULL;                                               \
    Int16 tagNameLength = 0;                                            \
                                                                        \
    /* Always read the type (if it has one) */                          \
    if( hasType ) {                                                     \
        ReadIntegerNumber( in, tagType );                               \
        dbAssert(tagType == Type, "Not reading a "#Type );              \
        /*dbPrintToConsole( "Handling a "#Type );*/                     \
        (void)tagType;                                                  \
    }                                                                   \
                                                                        \
    /* Always read the length of the name (if it has one) */            \
    if( hasName ) {                                                     \
        ReadIntegerNumber( in, tagNameLength );                         \
        /*dbPrintToConsole( "\ttagNameLength: ", tagNameLength );*/     \
                                                                        \
        /* If we're ignoring the tag then skip the name */              \
        if( shouldIgnore ) {                                            \
            Skip( in, tagNameLength );                                  \
                                                                        \
        /* Otherwise read it into a buffer on the heap */               \
        } else {                                                        \
            ReadText( in, tagName, tagNameLength );                     \
            /*dbPrintToConsole( "\ttagName: ", tagName );*/             \
        }                                                               \
    }

/*
 * Similar to the COMMON_HEADER macro above, this macro does common
 * cleanup. It is also super hacky.
 */
#define COMMON_FOOTER()                                                 \
    /* It we ignored the name then the pointer is NULL */               \
    delete [] tagName;

/*
 * Look at, but don't read, the next 8 bits of the stream
 * to detect the type of the upcoming tag.
 */
Int8 NBTParser::PeekNextTagType( std::istream& in ) {
    return in.peek();
}

/*
 * This function delegates to all the specific parsing functions
 */
void NBTParser::ParseTag( Int8 tagType, std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    switch( tagType ) {
        case TAG_End:        ParseTAG_End       ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Byte:       ParseTAG_Byte      ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Short:      ParseTAG_Short     ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Int:        ParseTAG_Int       ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Long:       ParseTAG_Long      ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Float:      ParseTAG_Float     ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Double:     ParseTAG_Double    ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Byte_Array: ParseTAG_Byte_Array( in, shouldIgnore, hasName, hasType ); break;
        case TAG_String:     ParseTAG_String    ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_List:       ParseTAG_List      ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Compound:   ParseTAG_Compound  ( in, shouldIgnore, hasName, hasType ); break;
        case TAG_Int_Array:  ParseTAG_Int_Array ( in, shouldIgnore, hasName, hasType ); break;
        default: dbCrash( "Unknown tag type ", static_cast<int>(tagType) );             break;
    }
}

/*
 * Parse data for a TAG_End
 */
void NBTParser::ParseTAG_End( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // TAG_End never has a name
    hasName = false;

    // Do all the common stuff
    COMMON_HEADER(TAG_End);
    COMMON_FOOTER();
}

/*
 * Parse data for a TAG_Byte
 */
void NBTParser::ParseTAG_Byte( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // Do all the common stuff
    COMMON_HEADER(TAG_Byte);

    // Read the 8-bit signed integer
    Int8 number;
    ReadIntegerNumber(in, number);

    // Check if it is any of the tags we are watching for
    if( shouldIgnore == false && hasName == true ) {
        if( strcmp(tagName, TagY) == 0 ) {
            currentSection = number;
        }
    }

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value: ", static_cast<Int64>(number) );
}

/*
 * Parse data for a TAG_Short
 */
void NBTParser::ParseTAG_Short( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Shorts
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_Short);

    // Skip reading the 16-bit signed integer (stored in big-endian format)
    Int16 number;
    Skip( in, sizeof(number) );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value: ", number );
}

/*
 * Parse data for a TAG_Int
 */
void NBTParser::ParseTAG_Int( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // Do all the common stuff
    COMMON_HEADER(TAG_Int);

    // Read the 32-bit signed integer (stored in big-endian format)
    Int32 number;
    ReadIntegerNumber( in, number );

    // Check if it is any of the tags we are watching for
    if( shouldIgnore == false && hasName == true ) {
        if( strcmp(tagName, TagXPos) == 0 ) {
            dbAssert( chunk.GetLocalPosition().x == number, "Loading data for wrong Chunk!" );
        } else if( strcmp(tagName, TagZPos) == 0 ) {
            dbAssert( chunk.GetLocalPosition().y == number, "Loading data for wrong Chunk!" );
        }
    }

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value: ", number );
}

/*
 * Parse data for a TAG_Long
 */
void NBTParser::ParseTAG_Long( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Longs
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_Long);

    // Skip reading the 64-bit signed integer (stored in big-endian format)
    Int64 number;
    Skip( in, sizeof(number) );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value: ", number );
}

/*
 * Parse data for a TAG_Float
 */
void NBTParser::ParseTAG_Float( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Floats
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_Float);

    // Skip reading the 32-bit floating point number
    // Right now this doesn't read properly, so we read a 32-bit int instead
    // Since we never use the value, it doesn't matter
    //Float32 number;
    Int32 fakeNumber;
    Skip( in, sizeof(fakeNumber) );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value (WRONG): ", fakeNumber );
}

/*
 * Parse data for a TAG_Double
 */
void NBTParser::ParseTAG_Double( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Doubles
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_Double);

    // Skip reading the 64-bit floating point number
    // Right now this doesn't read properly, so we read a 64-bit int instead
    // Since we never use the value, it doesn't matter
    //Float64 number;
    Int64 fakeNumber;
    Skip( in, sizeof(fakeNumber) );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tTag value (WRONG): ", fakeNumber );
}

/*
 * Parse data for a TAG_Byte_Array
 */
void NBTParser::ParseTAG_Byte_Array( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // Do all the common stuff
    COMMON_HEADER(TAG_Byte_Array);

    // Read the size of the array
    Int32 byteArraySize;
    ReadIntegerNumber( in, byteArraySize );

    // If we're ignoring the tag then skip all the bytes
    if( shouldIgnore ) {
        Skip( in, byteArraySize );
    }
    // Otherwise read everything into a buffer on the heap
    else {
        Int8* byteArray = new Int8[byteArraySize];

        in.read( reinterpret_cast<char*>(byteArray), byteArraySize );
        dbAssert( !in.fail(), "Unable to read bytes" );

        // Check if it is any of the tags we are watching for
        if( hasName == true ) {
            if( strcmp(tagName, TagBlocks) == 0 ) {
                chunk.SetBlockTypes( byteArray, byteArraySize, currentSection );
            }
        }

        delete [] byteArray;
    }

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tArray size: ", byteArraySize );
}

/*
 * Parse data for a TAG_String
 */
void NBTParser::ParseTAG_String( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Strings
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_String);

    // Read the length of the textstring
    Int16 textLength;
    ReadIntegerNumber( in, textLength );

    // And skip that much
    Skip( in, textLength );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tArray size: ", textLength );
}

/*
 * Parse data for a TAG_List
 */
void NBTParser::ParseTAG_List( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // Do all the common stuff
    COMMON_HEADER(TAG_List);

    // Read the type of tag the list holds
    Int8 listType;
    ReadIntegerNumber( in, listType );

    // Read the size of the list
    Int32 listSize;
    ReadIntegerNumber( in, listSize );

    // Parse all the tags in the list
    for( Int32 i = 0; i < listSize; ++i ) {
        ParseTag( listType, in, shouldIgnore, /*hasName*/false, /*hasType*/false );
    }

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tList size: ", listSize );
}

/*
 * Parse data for a TAG_Compound
 */
void NBTParser::ParseTAG_Compound( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // Do all the common stuff
    COMMON_HEADER(TAG_Compound);

    // Read all the sub-tags
    while( true ) {
        Int8 nextTagType = PeekNextTagType(in);

        ParseTag( nextTagType, in, shouldIgnore, /*hasName*/true, /*hasType*/true );

        // If it was a TAG_End then we're done
        if( nextTagType == TAG_End ) {
            break;
        }
    }

    // Do all the common stuff
    COMMON_FOOTER();
}

/*
 * Parse data for a TAG_Int_Array
 */
void NBTParser::ParseTAG_Int_Array( std::istream& in, bool shouldIgnore, bool hasName, bool hasType )
{
    // We never care about any Tag_Int_Arrays
    shouldIgnore = true;

    // Do all the common stuff
    COMMON_HEADER(TAG_Int_Array);

    // Read the size of the array
    Int32 arraySize;
    ReadIntegerNumber( in, arraySize );

    // And skip that much
    Skip( in, arraySize * sizeof(Int32) );

    // Do all the common stuff
    COMMON_FOOTER();

    //dbPrintToConsole( "\tArray size: ", arraySize );
}
