/*
 * This file implements an entire
 * Minecraft map loading in memory
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: July 31, 2014
 */

#include "World/Minecraft/Map.hpp"
#include "World/Minecraft/Region.hpp"
#include "World/Minecraft/Chunk.hpp"
#include "World/Minecraft/ParsingFunctions.hpp"
#include "World/Player/Player.hpp"
#include "Graphics/Camera/Camera.hpp"
#include "Graphics/OpenGL/Shader/Block/BlockShader.hpp"
#include "DebugTools/Debug.hpp"
#include <zlib/zlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>

/*
 * Constructor.
 * Takes in the name of the folder where the map data is stored as well as a reference
 * to the Player that the Map is relative to (e.g. draw all the Chunks that are near
 * the Player)
 */
Map::Map( std::string name, const Player& relativeTo )
    : mapName( name )
    , player( relativeTo )
    , mapCenter( 0, 0 )
    , chunkLoader( mapName, mapCenter )
{
    static_assert( LoadDistance >= DrawDistance, "Drawing more than is loaded" );
}

/*
 * Destructor.
 */
Map::~Map()
{
    // Since we're using forward declared types with std::unique_ptr we have explcitly declare
    // the Map destructor. Gotta love C++ ...
    // See -> http://stackoverflow.com/questions/9020372/how-do-i-use-unique-ptr-for-pimpl
    // And -> http://stackoverflow.com/questions/5460186/preventing-header-explosion-in-c-or-c0x
}

/*
 * Get the Chunk located at the given coordinates.
 * NULL is returned under the following circumstances:
 *     1) The coordinates are out of range (i.e. more than LoadDistance
 *        away from the center of the Map).
 *
 *     2) The coordinates are within range but the Chunk has not been loaded yet.
 *        Subsequents call may return a valid pointer (i.e. if the Chunk has been
 *        loaded in the meantime).
 *
 * If deferredLoad == true then a request to load the Chunk will be issued in the
 * event that the Chunk hasn't been loaded yet.
 */
const Chunk* Map::GetChunk( ChunkCoord coords, bool deferredLoad ) const
{
    // Find where in the 2D array the Chunk would be located
    glm::ivec2 index = GetArrayIndex( coords );

    // If it's out of range return NULL
    if( IsValidArrayIndex(index) == false ) {
        return nullptr;
    }

    // If Chunk is within range and loaded return it
    if( chunks[index.x][index.y] ) {
        return chunks[index.x][index.y].get();
    }
    // Otherwise add to a list of Chunks waiting to be loaded (if requested) and return NULL
    else {
        if( deferredLoad ) {
            chunkLoader.DeferredLoad(coords);
        }

        return nullptr;
    }
}

/*
 * Return true if the given position is inside a solid Block.
 */
bool Map::Colliding( WorldCoord pos ) const
{
    // If the Chunk the position is located in has been loaded
    if( const Chunk* chunk = GetChunk( pos.ToChunk() ) )
    {
        // Return if the Block the position is located in is solid
        return chunk->GetBlock( LocalBlockCoord(pos.ToBlock(), pos.ToChunk()) ).ShouldDraw();
    }
    // Otherwise we can't be colliding
    else {
        return false;
    }
}

/*
 * Calculate the array index where the Chunk with the given coordinates would be stored.
 * THIS MAY BE OUTSIDE THE BOUNDS OF THE ARRAY, MAKE SURE TO VERIFY INDEX IS VALID BEFORE USING!!
 */
glm::ivec2 Map::GetArrayIndex( ChunkCoord coords ) const
{
    return coords - mapCenter + glm::ivec2( int(LoadDistance), int(LoadDistance) );
}

/*
 * Return true if the given index is can be used to index the 2D array of loaded Chunks
 */
bool Map::IsValidArrayIndex( glm::ivec2 index ) const
{
    return index.x >= 0 && index.x < ChunkArraySize &&
           index.y >= 0 && index.y < ChunkArraySize;
}

/*
 * Move the Map's center based on Player location, loading/unloading Chunks as necessary
 */
void Map::Update( float deltaT )
{
    // We only have to shift the Map if the Player has moved far enough away from the center
    ChunkCoord playerChunk    = player.GetPosition().ToChunk();
    glm::ivec2 mapShiftAmount = mapCenter - playerChunk;

    if( std::abs(mapShiftAmount.x) < MaxOffset ) {
        mapShiftAmount.x = 0;
    } else {
        mapShiftAmount.x += (mapShiftAmount.x < 0) ? MaxOffset : -MaxOffset;
    }

    if( std::abs(mapShiftAmount.y) < MaxOffset ) {
        mapShiftAmount.y = 0;
    } else {
        mapShiftAmount.y += (mapShiftAmount.y < 0) ? MaxOffset : -MaxOffset;
    }

    mapCenter -= mapShiftAmount;

    // We move the Chunk pointers to a temporary buffer so that we don't
    // have to worry about overwriting pointers that haven't been shifted yet
    std::unique_ptr<Chunk> temp[ChunkArraySize][ChunkArraySize];

    for( int y = 0; y < ChunkArraySize; ++y ) {
        for( int x = 0; x < ChunkArraySize; ++x ) {
            glm::ivec2 newIndex = glm::ivec2(x, y) + mapShiftAmount;

            // If the Chunk is shifted off the Map then delete it
            if( IsValidArrayIndex( newIndex ) == false ) {
                chunks[x][y].reset();
            }
            // Otherwise move it to the shifted position in the buffer
            else {
                temp[newIndex.x][newIndex.y] = std::move( chunks[x][y] );
            }
        }
    }

    // Move the shifted pointers back into the original buffer
    for( int y = 0; y < ChunkArraySize; ++y ) {
        for( int x = 0; x < ChunkArraySize; ++x ) {
            chunks[x][y] = std::move( temp[x][y] );

            // If the Chunk exists then update the counter for time since loaded
            // FIXME: We may want to reset this counter if Chunks go out of view but aren't unloaded
            if( chunks[x][y] ) {
                chunks[x][y]->IncreaseTimeLoaded( deltaT );
            }
        }
    }

    // Grab any loaded Chunks and place them into the map.
    chunkLoader.SetOrigin( player.GetPosition().ToChunk() );
    chunkLoader.Update();

    auto loadedChunks = chunkLoader.GetLoadedChunks();

    for( auto& chunk : loadedChunks )
    {
        // And find where the Chunk should be located
        glm::ivec2 requestedChunkIndex = GetArrayIndex( chunk->GetLocalPosition() );

        // If the requested Chunk is now out of ranged ignore the request
        if( IsValidArrayIndex(requestedChunkIndex) == false ) {
            chunk.reset();
            continue;
        }

        // If the requested Chunk has already been loaded ignore the request
        dbAssert( chunks[requestedChunkIndex.x][requestedChunkIndex.y] == false, "Chunk already loaded!" );

        chunks[requestedChunkIndex.x][requestedChunkIndex.y] = std::move( chunk );
    }

    loadedChunks.clear();

    dbPrintToScreen( "Map Center : ", mapCenter  );
    dbPrintToScreen( "Offset     : ", playerChunk - mapCenter );
}

/*
 * Draw all the loaded Chunks that are close to the Player, from the
 * perspective of the given Camera
 */
void Map::Draw( const Camera& camera, glm::vec3 sunlightDir, float lightIntensity ) const
{
    static BlockShader shader(
        "Source/Graphics/OpenGL/Shader/Block/BlockShader.vert",
        "Source/Graphics/OpenGL/Shader/Block/BlockShader.frag"
    );

    int numVertices = 0;
    const float fadeInTime = 1.0f;      // 1 second
    WorldCoord playerPos = player.GetPosition();
    ChunkCoord playerChunk = playerPos.ToChunk();

    // Get a list of loaded Chunks that are "near" the Player
    std::vector<const Chunk*> chunksToBeDrawn;

    for( int y = -DrawDistance; y <= DrawDistance; ++y ) {
        for( int x = -DrawDistance; x <= DrawDistance; ++x )
        {
            // If the nearby Chunk exists add it to to the list
            ChunkCoord chunkCoords = playerChunk + glm::ivec2(x, y);

            if( const Chunk* currentChunk = GetChunk(chunkCoords) )
            {
                glm::vec3 minCorner = currentChunk->GetGlobalPosition();
                glm::vec3 maxCorner = minCorner + glm::vec3(Chunk::SizeX, Chunk::SizeY, Chunk::SizeZ);

                if( camera.IsAABBInView(minCorner, maxCorner) )
                {
                    chunksToBeDrawn.push_back( currentChunk );
                    numVertices += currentChunk->GetNumVertices();
                }
            }
        }
    }

    // Sort all the nearby Chunks by distance (nearest first) to the Player
    std::sort( chunksToBeDrawn.begin(), chunksToBeDrawn.end(),
        [&](const Chunk* A, const Chunk* B) -> bool {
            return A->DistanceFrom(playerPos) < B->DistanceFrom(playerPos);
        }
    );

    // Bind the shader and texture
    shader.Bind();
    Block::BindTexture();
    shader.SetSunlightDirection( sunlightDir );
    shader.SetSunlightIntensity( lightIntensity );

    // We draw the opaque Blocks front-to-back so that we minimize overdraw
    for( auto chunk = chunksToBeDrawn.cbegin(); chunk != chunksToBeDrawn.cend(); ++chunk )
    {
        // Set the scene matrix based on the camera viewpoint
        glm::mat4 MVP = camera.GetViewProjectionMatrix() * (**chunk).GetModelMatrix();
        shader.SetMVPMatrix( MVP );

        float fadeAmount = std::min(1.0f, (**chunk).GetTimeLoaded() / fadeInTime );
        shader.SetAlpha( fadeAmount );

        (**chunk).DrawOpaqueBlocks();
    }

    // We draw the translucent Blocks back-to-front so that things are blended properly
    glDepthMask(GL_FALSE);
    for( auto chunk = chunksToBeDrawn.crbegin(); chunk != chunksToBeDrawn.crend(); ++chunk )
    {
        // Set the scene matrix based on the camera viewpoint
        glm::mat4 MVP = camera.GetViewProjectionMatrix() * (**chunk).GetModelMatrix();
        shader.SetMVPMatrix( MVP );

        float fadeAmount = std::min(1.0f, (**chunk).GetTimeLoaded() / fadeInTime );
        shader.SetAlpha( fadeAmount );

        // Draw the Chunk
        (**chunk).DrawTranslucentBlocks();
    }
    glDepthMask(GL_TRUE);

    // Unbind everything
    sf::Texture::bind( nullptr );
    shader.Unbind();

    dbPrintToScreen( "Rendering ", numVertices, " vertices" );
}
