/*
 * This file implements a single 16
 * block wide, 16 block long, and
 * 256 block deep chunk
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: August 5, 2013
 */

#include "World/Minecraft/Chunk.hpp"
#include "World/Minecraft/NBTParser.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <cstring>
#include <type_traits>
#include <cstddef>

/*
 * Constructor. Takes in a blob of binary data that needs to be parsed
 * to create the Chunk. Creates an empty Chunk if given no data.
 */
Chunk::Chunk( ChunkCoord coords, unsigned long length, unsigned char* data )
    : xPos( coords.x )
    , zPos( coords.y )
    , hasData( data != nullptr )
    , timeLoaded( 0.0f )
{
    // If given actual data then read it. Otherwise the Chunk will stay
    // in its default state, which is full of Air Blocks
    if( hasData ) {
        NBTParser parser( *this );
        parser.Parse( data, length );
    }

    // Create the graphical model from the Block data
    CreateModel();

    dbPrintToConsole( "Created Chunk at ", GetLocalPosition() );
    //dbPrintToConsole( "    Pos          : (", xPos, "x, ", zPos, "z)" );
    //dbPrintToConsole( "    Num Vertices : ", GetNumVertices() );
    //dbPrintToConsole( "    VBO Size (KB): ", model.GetVBOSize() / 1024.0f );
}

/*
 * Destructor
 */
Chunk::~Chunk()
{
    dbPrintToConsole( "Destroyed Chunk at ", GetLocalPosition() );
}
/*
 * Create the graphical model for the Chunk based on the Block data.
 * Will attempt to cull any Blocks that can't possibly be seen.
 */
void Chunk::CreateModel()
{
    // FIXME: No need to create model or draw Chunk if hasData == false;

    // The number of expected quads is from experimental data
    opaqueBlockModel.SetExpectedQuadNum( 17000 );
    translucentBlockModel.SetExpectedQuadNum( 17000 );

    // FIXME: This should probably be a static variable in some rand() function
    //std::random_device rd;
    std::default_random_engine gen;
    std::uniform_real_distribution<float> dis(-0.5, 0.5);

    // For every Block in the Chunk
    for( int y = 0; y < SizeY; ++y ) {
        for( int z = 0; z < SizeZ; ++z ) {
            for( int x = 0; x < SizeX; ++x ) {
                const Block& block = GetBlock(x, y, z);
                WorldCoord blockPos(x, y, z);   // Not BlockCoord because some Blocks (e.g. grass) are not aligned to the grid

                // Completely skip blocks that aren't drawn (e.g. Air)
                if( block.ShouldDraw() == false ) {
                    continue;
                }

                // Translucent Blocks have their own VBO
                // Water is drawn slightly depressed, so you can see a lip on shore
                //
                // FIXME: This is a very hacky solution since it should only be applied to water
                //       but it applied to all Blocks that need alpha blending (e.g. ice).
                //       Also only drawing the top quad of the water Block will look bad if the Camera
                //       is allowed to go underwater.
                if( block.RequiresAlphaBlending() ) {
                    block.AddTopFace( blockPos - glm::vec3(0, 0.125f, 0), translucentBlockModel );
                    continue;
                }

                // If the block is an entity (e.g. grass or flowers)
                // Almost all entity blocks won't be completely surrounded
                // by other blocks so we won't bother checking for now
                // There is a random offset added to that things don't look quite so grid-like
                //
                // FIXME: This is a very hacky solution since it should only be applied to grass
                //       but it applied to all entities. This breaks things like sugarcane
                // FIXME: They are also lowered slightly, this breaks things like lilypads
                if( block.IsCrossShaped() || block.IsFlat() ) {
                    block.AddAllFaces( blockPos + glm::vec3(dis(gen), -0.125f, dis(gen)), opaqueBlockModel );
                    continue;
                }

                // Otherwise the Block is a cube
                dbAssert( block.IsCubeShaped(), "Block isn't a cube, missed a case above" );

                // Check if can cull left face
                if( x == 0 || GetBlock(x - 1, y, z).CompletelySolid() == false ) {
                    block.AddLeftFace( blockPos, opaqueBlockModel );
                }

                // Check if can cull right face
                if( x == SizeX - 1 || GetBlock(x + 1, y, z).CompletelySolid() == false ) {
                    block.AddRightFace( blockPos, opaqueBlockModel );
                }

                // Check if can cull back face
                if( z == 0 || GetBlock(x, y, z - 1).CompletelySolid() == false ) {
                    block.AddBackFace( blockPos, opaqueBlockModel );
                }

                // Check if can cull front face
                if( z == SizeZ - 1 || GetBlock(x, y, z + 1).CompletelySolid() == false ) {
                    block.AddFrontFace( blockPos, opaqueBlockModel );
                }

                // Check if can cull bottom face
                if( y == 0 || GetBlock(x, y - 1, z).CompletelySolid() == false ) {
                    block.AddBottomFace( blockPos, opaqueBlockModel );
                }

                // Check if can cull top face
                if( y == SizeY - 1 || GetBlock(x, y + 1, z).CompletelySolid() == false ) {
                    block.AddTopFace( blockPos, opaqueBlockModel );
                }
            }
        }
    }

    // Copy data into VBO
    opaqueBlockModel.CopyToVBO( GL_STATIC_DRAW );
    translucentBlockModel.CopyToVBO( GL_STATIC_DRAW );
}

/*
 * Draw all the opaque Blocks in the Chunk
 */
void Chunk::DrawOpaqueBlocks() const
{
    opaqueBlockModel.Draw();
}

/*
 * Draw all the translucent Blocks in the Chunk
 */
void Chunk::DrawTranslucentBlocks() const
{
    translucentBlockModel.Draw();
}

// FIXME: Return reference rather than object
const glm::mat4 Chunk::GetModelMatrix() const
{
    glm::mat4 modelMatrix( 1.0f );
    modelMatrix = glm::translate( modelMatrix, GetGlobalPosition() );
    return modelMatrix;
}

/*
 * Set the Block data for the given 16x16x16 section of the Chunk
 */
void Chunk::SetBlockTypes( Int8* data, Int32 length, Int8 section )
{
    dbAssert( section >= 0 || section < 16, "Not a valid section! Given ", section );
    dbAssert( length == 16 * 16 * 16, "Array wrong size! Given ", length );
    (void)length;

    // FIXME: std::is_trivially_copyable<T> is what we really want to check, but GCC 4.7 doesn't implement it.
    //static_assert( std::is_trivially_copyable<Block>::value == true, "Cannot use memcpy to copy data" );
    static_assert( std::is_standard_layout<Block>::value == true, "Cannot use memcpy to copy data" );

    memcpy( blockArr + GetArrayIndex(0, section * 16, 0), data, length );

    // Since the Block class is so simple we can use memcpy rather than an explicit loop
    // Keeping this example around if we add more member variables to the Block class
    // and we have to go back to using the loop.
    /*
    for( int y = 0; y < 16; ++y ) {
        for( int z = 0; z < 16; ++z ) {
            for( int x = 0; x < 16; ++x ) {
                GetBlock(x, y + (section * 16), z).SetType( *data );
                data++;
            }
        }
    }
    */
}
