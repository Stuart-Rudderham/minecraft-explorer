/*
 * This header defines a parser for
 * a file in the Named-Binary-Tag (NBT)
 * format
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: August 27, 2013
 */

#ifndef NBTPARSER_HPP_INCLUDED
#define NBTPARSER_HPP_INCLUDED

/*
 * Some references for the NBT format are:
 *     1) http://www.minecraftwiki.net/wiki/NBT_Format
 *     2) http://wiki.vg/NBT
 *     3) http://web.archive.org/web/20110723210920/http://www.minecraft.net/docs/NBT.txt
 */

// FIXME: This class is pretty much only for reading Chunk data. It should
// be refactored if we ever want to read other NBT files or fields (e.g. if
// we ever want to read the spawn point location for the map).
//
// Think hard about the refactor though, as currently this is stipped
// down to the bare essentials for parsing Chunk data, which means
// it's pretty fast. Any additional functionality would probably negatively
// impact Chunk loading time.

#include "BasicTypes.hpp"
#include <iosfwd>

class Chunk;

class NBTParser {
    public:
        NBTParser( Chunk& c );                                              // Ctor. Takes in a reference to the Chunk
                                                                            // whos data we will be parsing.

        void Parse( unsigned char* data, unsigned long length );            // Parse the given binary blob of data for
                                                                            // the Chunk data.
    private:
        Chunk& chunk;                                                       // The Chunk whos data we're reading

        Int8 currentSection;                                                // The current 16x16x16 section of the Chunk
                                                                            // we are reading.

        // All the parsing functions.
        Int8 PeekNextTagType( std::istream& in );

        void ParseTag( Int8 tagType, std::istream& in, bool shouldIgnore, bool hasName, bool hasType );

        void ParseTAG_End          ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Byte         ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Short        ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Int          ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Long         ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Float        ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Double       ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Byte_Array   ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_String       ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_List         ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Compound     ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
        void ParseTAG_Int_Array    ( std::istream& in, bool shouldIgnore, bool hasName, bool hasType );
};

#endif // NBTPARSER_HPP_INCLUDED
