/*
 * This file implements a player that
 * can explore the Minecraft world
 *
 * Author:      Stuart Rudderham
 * Created:     March 18, 2013
 * Last Edited: August 24, 2013
 */

#include "World/Player/Player.hpp"
#include "Graphics/Camera/Camera.hpp"
#include "DebugTools/Debug.hpp"
#include "Input/InputManager.hpp"
#include "Input/Input.hpp"
#include <cmath>

#ifndef M_PI
    #define M_PI    3.14159265358979323846f
#endif

/*
 * Constructor. Creates a Player at (0, 0, 0) looking in the direction (0, 0, -1).
 */
Player::Player()
    : position(0.0f, 0.0f, 0.0f)
    , velocity(0.0f, 0.0f, 0.0f)
    , pitchRotation(0.0f)
    , yawRotation(0.0f)
    , forwardKey ( InputManager::Get().ListenFor( sf::Keyboard::W      ) )
    , backwardKey( InputManager::Get().ListenFor( sf::Keyboard::S      ) )
    , leftKey    ( InputManager::Get().ListenFor( sf::Keyboard::A      ) )
    , rightKey   ( InputManager::Get().ListenFor( sf::Keyboard::D      ) )
    , upKey      ( InputManager::Get().ListenFor( sf::Keyboard::Space  ) )
    , downKey    ( InputManager::Get().ListenFor( sf::Keyboard::LShift ) )
{

}

/*
 * Update the Player's view direction in the horizonal plane (i.e. around the Y axis).
 */
void Player::LookLeftRight( float delta ) {
    yawRotation += delta;
    yawRotation = CorrectMod( yawRotation, 360.0f );
}

/*
 * Update the Player's view direction in the vertical plane (i.e. around the X axis).
 */
void Player::LookUpDown( float delta ) {
    pitchRotation += delta;
    pitchRotation = glm::clamp( -89.0f, pitchRotation, 89.0f );
}

/*
 * Set the Player's forward/backward velocity.
 * Based on the view direction.
 */
void Player::SetForwardVelocity( float speed )
{
    velocity.x +=  speed * sinf(yawRotation * M_PI / 180.0f);
    velocity.z += -speed * cosf(yawRotation * M_PI / 180.0f);
}

/*
 * Set the Player's left/right velocity.
 * Based on the horizontal vector perpendicular to the view direction.
 */
void Player::SetStrafeVelocity( float speed )
{
    velocity.x += speed * cosf(yawRotation * M_PI / 180.0f);
    velocity.z += speed * sinf(yawRotation * M_PI / 180.0f);
}

/*
 * Set the Player's up/down velocity.
 * Is easy because it doesn't depend on view direction.
 */
void Player::SetVerticalVelocity( float speed )
{
    velocity.y += speed;
}

/*
 * Update the Player's position and view direction based on the
 * amount of time that has passed and how far the mouse has moved.
 */
void Player::Update( float deltaT, glm::vec2 mouseDelta )
{
    const float speed = 5;
    const float lookSensitivity = 10.0f;
    const float friction = 0.75f;

    // Move the view direction based on mouse movement
    LookLeftRight( (float)mouseDelta.x / lookSensitivity );
    LookUpDown   ( (float)mouseDelta.y / lookSensitivity );

    // Forward movement
    if( forwardKey->IsDown() ) {
        SetForwardVelocity( speed );
    } else if( backwardKey->IsDown() ) {
        SetForwardVelocity( -speed );
    }

    // Strafe movement
    if( leftKey->IsDown() ) {
        SetStrafeVelocity( -speed );
    } else if( rightKey->IsDown() ) {
        SetStrafeVelocity( speed );
    }

    // Vertical movement
    if( upKey->IsDown() ) {
        SetVerticalVelocity( speed );
    } else if( downKey->IsDown() ) {
        SetVerticalVelocity( -speed );
    }

    // Update the position and velocity

    // FIXME: Don't do collision here!!!
    /*
    if( map.Colliding( position + velocity * deltaT ) ) {
        velocity = glm::vec3(0, 0, 0);
    }
    */

    position += velocity * deltaT;
    velocity = velocity * friction;

    std::string direction = "";
    if( yawRotation < 45.0f || yawRotation >= 315.0f ) {
        direction = "South";
    }
    else if( yawRotation >= 45.0f && yawRotation < 135.0f ) {
        direction = "East";
    }
    else if( yawRotation >= 135.0f && yawRotation < 225.0f ) {
        direction = "North";
    }
    else if( yawRotation >= 225.0f && yawRotation < 315.0f ) {
        direction = "West";
    }

    dbPrintToScreen( "Direction    : ", direction               );
    dbPrintToScreen( "Yaw          : ", yawRotation             );
    dbPrintToScreen( "Pitch        : ", pitchRotation           );
    dbPrintToScreen( "Global Pos   : ", position                );
    dbPrintToScreen( "Global Block : ", position.ToBlock()      );
    dbPrintToScreen( "Region       : ", position.ToRegion()     );
    dbPrintToScreen( "Chunk        : ", position.ToChunk()      );
    dbPrintToScreen( "Local Block  : ", position.ToLocalBlock() );
}

/*
 * Modify the provided Camera so that it is located at the Player's
 * position and points in the direction the Player is facing
 */
void Player::SetCamera( Camera& camera )
{
    glm::vec3 viewDir;

    viewDir.x =  sinf(yawRotation   * M_PI / 180.0f) * cosf(pitchRotation * M_PI / 180.0f);
    viewDir.y =  sinf(pitchRotation * M_PI / 180.0f);
    viewDir.z = -cosf(yawRotation   * M_PI / 180.0f) * cosf(pitchRotation * M_PI / 180.0f);

    camera.SetViewDirection( viewDir );
    camera.MoveTo( position );

    dbPrintToScreen( "View  : ", viewDir );
}
