/*
 * This header defines a player that
 * can explore the Minecraft world
 *
 * Author:      Stuart Rudderham
 * Created:     March 15, 2013
 * Last Edited: August 24, 2013
 */

#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED

#include "Singleton.hpp"
#include "World/CoordinateSystem.hpp"
#include <glm/glm.hpp>

// Forward declarations
class KeyboardState;
class Camera;

class Player : public Singleton<Player> {
    public:
        Player();                                               // ctor. Creates a Player at (0, 0, 0) looking
                                                                // in the direction (0, 0, -1)

        void Update( float deltaT, glm::vec2 mouseDelta );      // Update the Player's position
        void SetCamera( Camera& camera );

        inline WorldCoord GetPosition() const {
            return position;
        }

        inline void MoveTo( glm::vec3 newPos ) {                // Setters for the position
            position = newPos;
        }

        inline void MoveBy( glm::vec3 delta ) {
            position = position + delta;
        }

        void SetForwardVelocity( float speed );                 // Setters for the velocity
        void SetStrafeVelocity( float speed );
        void SetVerticalVelocity( float speed );

        void LookLeftRight( float delta );                      // Setters for the view direction
        void LookUpDown( float delta );

    private:
        WorldCoord position;                                    // The Player's current position (in world coords)
        glm::vec3 velocity;                                     // The Player's current velocity

        float pitchRotation;                                    // The pitch of the Player's view (rotation around x-axis) in degrees
        float yawRotation;                                      // The yaw of the Player's view (rotation around the y-axis) in degrees

        const KeyboardState* forwardKey;                        // Control scheme
        const KeyboardState* backwardKey;
        const KeyboardState* leftKey;
        const KeyboardState* rightKey;
        const KeyboardState* upKey;
        const KeyboardState* downKey;
};

#endif // PLAYER_HPP_INCLUDED
