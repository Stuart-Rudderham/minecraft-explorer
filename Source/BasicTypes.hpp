/*
 * This header defines basic types
 * and typedefs
 *
 * Author:      Stuart Rudderham
 * Created:     January 23, 2013
 * Last Edited: May 18, 2013
 */

#ifndef BASICTYPES_HPP_INCLUDED
#define BASICTYPES_HPP_INCLUDED

#include <climits>

// Typedefs to get the correct data width
typedef signed   char       Int8;
typedef unsigned char       UInt8;

typedef signed   short      Int16;
typedef unsigned short      UInt16;

typedef signed   int        Int32;
typedef unsigned int        UInt32;

typedef signed   long long  Int64;
typedef unsigned long long  UInt64;

typedef float               Float32;
typedef double              Float64;

// Asserts to make sure that type sizes are as expected
static_assert( CHAR_BIT        == 8, "A byte is not 8 bits"   );

static_assert( sizeof(Int8)    == 1, "Int8 is not 8 bits"     );
static_assert( sizeof(UInt8)   == 1, "UInt8 is not 8 bits"    );

static_assert( sizeof(Int16)   == 2, "Int16 is not 16 bits"   );
static_assert( sizeof(UInt16)  == 2, "UInt16 is not 16 bits"  );

static_assert( sizeof(Int32)   == 4, "Int32 is not 32 bits"   );
static_assert( sizeof(UInt32)  == 4, "UInt32 is not 32 bits"  );

static_assert( sizeof(Int64)   == 8, "Int64 is not 64 bits"   );
static_assert( sizeof(UInt64)  == 8, "UInt64 is not 64 bits"  );

static_assert( sizeof(Float32) == 4, "Float32 is not 32 bits" );
static_assert( sizeof(Float64) == 8, "Float64 is not 64 bits" );

#endif // BASICTYPES_HPP_INCLUDED
