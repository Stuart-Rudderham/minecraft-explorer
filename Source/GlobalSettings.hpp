/*
 * This header defines a bunch of global
 * variables that are needed for the game
 *
 * Author:      Stuart Rudderham
 * Created:     August 24, 2013
 * Last Edited: August 24, 2013
 */

#ifndef GLOBALSETTINGS_HPP_INCLUDED
#define GLOBALSETTINGS_HPP_INCLUDED

float FOV;

glm::vec2 window_size



#endif // GLOBALSETTINGS_HPP_INCLUDED
