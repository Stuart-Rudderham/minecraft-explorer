#include "DebugTools/Debug.hpp"
#include "DebugTools/GraphicalString.hpp"
#include "Input/InputManager.hpp"
#include "Input/Input.hpp"
#include "World/Player/Player.hpp"
#include <GL/glew.h>
#include "World/World.hpp"
#include "World/Minecraft/Chunk.hpp"
#include "World/Minecraft/Map.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "DebugTools/TimeProfiler.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <zlib/zlib.h>
#include <exception>
#include "Graphics/Camera/Camera.hpp"
#include "World/CoordinateSystem.hpp"

/*
struct Counter : public Singleton<Counter> {
    Counter() : ctr(0) {}
    ~Counter() { dbAssert( ctr == 0, "Memory Leak! ctr = ", ctr ); }

    long long ctr;
};

void* operator new(std::size_t n) throw(std::bad_alloc)
{
    Counter::Get().ctr++;
    dbAssert(n != 20352, "Break");
    void* ptr = malloc(n);
    dbPrintToConsole(ptr, " - new of size ", n);

    return ptr;
}

void operator delete(void* p) throw()
{
    Counter::Get().ctr--;

    dbPrintToConsole(p, " - freed");

    free(p);
}
*/
bool shouldSort = true;

int main()
{
    // Create the window (in the middle of the screen) as well as the OpenGL context
    sf::Window gameWindow(
        sf::VideoMode(960, 540),
        "Minecraft Explorer",
        sf::Style::Default,
        sf::ContextSettings( 24, 8, 0 )
    );

    gameWindow.setPosition( sf::Vector2i( (sf::VideoMode::getDesktopMode().width  - gameWindow.getSize().x) / 2,
                                          (sf::VideoMode::getDesktopMode().height - gameWindow.getSize().y) / 2 ) );
    //gameWindow.setPosition( sf::Vector2i(0, 0) );
    //debugWindow.setPosition( {gameWindow.getSize().x, 0} );

    //gameWindow.setFramerateLimit(60);
    //gameWindow.setMouseCursorVisible(false);

    // Load and set the window icon
    sf::Image windowIcon;
    dbErrorCheck( windowIcon.loadFromFile("Content/Images/Icon.png"), "Unable to load window icon" );
    gameWindow.setIcon( windowIcon.getSize().x, windowIcon.getSize().y, windowIcon.getPixelsPtr() );

    // Initialize GLEW
    glewExperimental = true;
    dbErrorCheck( glewInit() == GLEW_OK, "Failed to initialize GLEW" );

    // Print out some information about the program
    dbPrintToConsole( "Compiled with GCC ", __GNUC__, ".", __GNUC_MINOR__, ".", __GNUC_PATCHLEVEL__ );
    dbPrintToConsole( "Using zlib version ", zlib_version );
    dbPrintToConsole( "Using GLEW version ", glewGetString(GLEW_VERSION) );
    dbPrintToConsole( "Using GLM version ", GLM_VERSION_MAJOR, ".", GLM_VERSION_MINOR, ".", GLM_VERSION_PATCH, ".", GLM_VERSION_REVISION );

    sf::ContextSettings settings = gameWindow.getSettings();
    dbPrintToConsole( "Using OpenGL version ", settings.majorVersion, ".", settings.minorVersion );
    dbPrintToConsole( "Antialiasing level = ", settings.antialiasingLevel );
    dbPrintToConsole( "Depth bits         = ", settings.depthBits );
    dbPrintToConsole( "Stencil bits       = ", settings.stencilBits );

    GLint texSize;
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, &texSize );
    dbPrintToConsole( "Max texture size   = ", texSize );

    // Initialize some general program variables
    bool shouldQuit = false;
    bool paused = false;
    bool hasFocus = true;

    // Setup inputs to listen to
    const KeyboardState* quitKey       = InputManager::Get().ListenFor( sf::Keyboard::Q );
    const KeyboardState* pauseKey      = InputManager::Get().ListenFor( sf::Keyboard::P );
    const KeyboardState* toggleCullKey = InputManager::Get().ListenFor( sf::Keyboard::E );

    // Timing stuff
    TimeProfiler globalFramerate( "Global Framerate" );

    // OpenGL stuff
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );
    glEnable( GL_CULL_FACE );

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    //glDisable( GL_MULTISAMPLE_ARB );

    //glEnable (GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable( GL_ALPHA_TEST );                          // If fragment has alpha of 0 (i.e. completely transparent)
    glAlphaFunc( GL_GREATER, 0.0f);                     // then don't draw it. This is useful because it means that
                                                        // we don't have to do sorting before drawing, since it is a
                                                        // binary yes/no choice.

    // Gameplay stuff
    Player player;                                      // The user-controlled Player that is used to explore the World.
    Camera playerCamera(                                // The Camera used to draw what the Player sees.
        /* FOV */ 40.0f,
        /* Aspect Ratio */ 960.0f / 540.0f,
        /* Near Clip Plane */ 0.01,
        /* Far Clip Plane */ 1000
    );

    Camera hudCamera(
        /* Left Clip Plane   */  0.0f,
        /* Right Clip Plane  */  960.0f,
        /* Bottom Clip Plane */  540.0f,
        /* Top Clip Plane    */  0.0f,
        /* Near Clip Plane   */ -1.0f,
        /* Far Clip Plane    */  1.0f
    );

    World world( "dtemple", player );                   // The World that the Player is exploring.

    // If uncaught expections are thrown it can be a hassle to find the
    // line that threw them. Set the handler functions to crash in a way
    // that a debugger can hook into, so that it is easier to debug.
    // The error mesage will have the wrong line number, since the __LINE__
    // macro is expanded right here. To find where the exception was thrown
    // use the debugger to walk up the call stack, looking for anything suspicious
    std::set_unexpected ( []() { dbCrash("Unexected handler");         } );
    std::set_terminate  ( []() { dbCrash("Terminate handler");         } );
    std::set_new_handler( []() { dbCrash("Failed to allocate memory"); } );

    // Move the mouse to the center so that there isn't a huge mouse delta in the first frame
    sf::Vector2u windowCenter = gameWindow.getSize() / 2u;
    sf::Mouse::setPosition( sf::Vector2i(windowCenter.x, windowCenter.y), gameWindow );

    // Assume 60fps initially
    float deltaT = 60.0f / 1000.0f;

    // Game loop
    while( !shouldQuit )
    {
        globalFramerate.Start();

        // Handle any events
        sf::Event event;
        while( gameWindow.pollEvent(event) ) {
            if( event.type == sf::Event::Closed ) {
                shouldQuit = true;
            }
            else if( event.type == sf::Event::Resized ) {
                glViewport( 0, 0, event.size.width, event.size.height );

                playerCamera.SetAspectRatio( (float)event.size.width / event.size.height );
                hudCamera.SetScreenSize( event.size.width, event.size.height );

                // Center the mouse so the view doesn't change
                glm::vec2 windowCenter( gameWindow.getSize().x / 2u, gameWindow.getSize().y / 2u );
                sf::Mouse::setPosition( sf::Vector2i(windowCenter.x, windowCenter.y), gameWindow );
            }
            else if( event.type == sf::Event::LostFocus ) {
                hasFocus = false;
            }
            else if( event.type == sf::Event::GainedFocus ) {
                hasFocus = true;

                // Center the mouse so the view doesn't change
                glm::vec2 windowCenter( gameWindow.getSize().x / 2u, gameWindow.getSize().y / 2u );
                sf::Mouse::setPosition( sf::Vector2i(windowCenter.x, windowCenter.y), gameWindow );
            }
            else if( event.type == sf::Event::MouseWheelMoved ) {
                world.AdjustTimeOfDay( event.mouseWheel.delta * 60 * 15 ); // Quarter hour increments
                //FOV += event.mouseWheel.delta;
                //playerCamera.SetFOV( FOV );
            }
        }

        // Handle input from the user
        InputManager::Get().UpdateInputs( gameWindow );

        if( quitKey->IsDown()         ) { shouldQuit = true;    }
        if( pauseKey->IsJustPressed() ) { paused     = !paused; }

        if( toggleCullKey->IsJustPressed() ) {
            shouldSort = !shouldSort;
        }

        // Reset mouse position and use it to rotate Player
        glm::vec2 mouseDelta( MouseState::GetPosition().x, MouseState::GetPosition().y );
        glm::vec2 windowCenter( gameWindow.getSize().x / 2u, gameWindow.getSize().y / 2u );

        mouseDelta -= windowCenter;
        mouseDelta.y *= -1.0f;      // Because the top of the screen is 0, so moving the mouse up is
                                    // substracting, which is the opposite of what we want
        if( hasFocus ) {
            sf::Mouse::setPosition( sf::Vector2i(windowCenter.x, windowCenter.y), gameWindow );
        } else {
            mouseDelta = glm::vec2( 0.0f, 0.0f );
        }

        if( paused ) {
            sf::sleep( sf::milliseconds(10) );
            continue;
        }

        // Update the Player's position and view direction
        player.Update( deltaT, mouseDelta );

        // Update the World
        world.Update( deltaT );

        // Move the Camera to the Player's new position
        // We do this after updating the World in case the Player's position
        // is adjusted (e.g. for collision detection)
        player.SetCamera( playerCamera );

        // Draw the scene
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                 // Clear the buffers

        world.Draw( playerCamera );

        glDisable( GL_CULL_FACE );
        dbDrawDebugText( hudCamera /*playerCamera*/ );
        glEnable( GL_CULL_FACE );
        //dbDrawDebugText( glm::vec2(10, 10) );

        // Flip buffers
        gameWindow.display();

        globalFramerate.Stop();

        dbPrintToScreen( "FPS       : ", 1000000.0f / globalFramerate.GetMostRecentTime() );
        dbPrintToScreen( "Frame time: ", globalFramerate.GetMostRecentTime() / 1000.0f );
        dbPrintToScreen( "DeltaT    : ", deltaT );
        deltaT = globalFramerate.GetMostRecentTime() / 1000000.0f;
    }

    return 0;
}
