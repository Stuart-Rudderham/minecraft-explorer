/*
 * This header defines some improvements to
 * the way input from the user is received
 *
 * Author:      Stuart Rudderham
 * Created:     February 3, 2012
 * Last Edited: May 5, 2013
 */

#ifndef SFML_INPUT_HPP_INCLUDED
#define SFML_INPUT_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Keyboard.hpp>

/*******************************************************
 *                     Base Class                      *
 *******************************************************/
/*
 * This is the base class that is used to store the state of
 * input from the user. Its child classes deal with input from
 * the Mouse and Keyboard, since these are handled slightly differently
 * by SFML. It is an abstract class, so it cannot be instantiated itself
 *
 * Note: check to make sure that the program window is in focus before
 *       updating any input, because the input will still update even
 *       if the window isn't in focus, which could cause strange behaviour
 */

class InputState {
    private:
        bool down;                              // this is true if the key/button is pressed down, regardless of how long it's been down
        bool justPressed;                       // this is true the first time the check has been done after the key has been pressed and false after that
        bool justReleased;                      // this is true the first time the check has been done after the key has been released and false after that

    protected:
        void InputIsDown();                     // sets the 3 booleans below based on the previous state and assuming the input is currently being pressed
        void InputIsNotDown();                  // sets the 3 booleans below based on the previous state and assuming the input is not currently being pressed

    public:
        InputState();
        virtual ~InputState() {}                // dtor doesn't do anything, need to declare and make it virtual though to avoid underfined behaviour when deleting though

        virtual void Update() = 0;

        // getter functions for the three states of the input
        inline bool IsDown()         const { return down;         }
        inline bool IsJustPressed()  const { return justPressed;  }
        inline bool IsJustReleased() const { return justReleased; }
};


/*******************************************************
 *                     Mouse Input                     *
 *******************************************************/
/*
 * The class used the handle input from a button on the mouse. It only
 * handles one button, so you need to instantiate one object per mouse
 * button (e.g. left click, right click, middle click)
 */
class MouseState : public InputState {
    private:
        sf::Mouse::Button button;                                                               // this is the mouse button the object monitors
        static sf::Vector2i position;                                                           // this is the position of the cursor

    public:
        MouseState( const sf::Mouse::Button& b );                                               // the constructor for the class, the only parameter is the button to be associated with the object

        void Update() /*override*/;                                                                 // update the 3 booleans based on the state of the button

        static void UpdateMouseCoords( sf::Window& window );                                    // update the location of the cursor
        static inline sf::Vector2i GetPosition() { return position; }                           // return the location of the cursor

    friend bool operator==( const MouseState& A, const MouseState& B );                         // equality operator needs to check private values
};

bool operator==( const MouseState& A, const MouseState& B );                                    // forward declare the equality operator


/*******************************************************
 *                  Keyboard Input                     *
 *******************************************************/
/*
 * The class used the handle input from a button on the mouse. It only
 * handles one button, so you need to instantiate one object per mouse
 * button (e.g. left click, right click, middle click)
 */
class KeyboardState : public InputState {
    private:
        sf::Keyboard::Key key;                                                                  // this is the key on the keyboard the object monitors

    public:
        KeyboardState( const sf::Keyboard::Key& k );											// the constructor for the class, the only parameter is the key to be associated with the object

        void Update() /*override*/;                                                                 // update the 3 booleans based on the state of the key

    friend bool operator==( const KeyboardState& A, const KeyboardState& B );                   // equality operator needs to check private values
};

bool operator==( const KeyboardState& A, const KeyboardState& B );                              // forward declare the equality operator

#endif // SFML_INPUT_HPP_INCLUDED
