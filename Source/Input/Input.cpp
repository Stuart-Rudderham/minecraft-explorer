/*
 * This file implements some improvements to
 * the way input from the user is received
 *
 * Author:      Stuart Rudderham
 * Created:     February 3, 2012
 * Last Edited: May 5, 2012
 */

#include "Input/Input.hpp"

/*******************************************************
 *                     Base Class                      *
 *******************************************************/

// The constructor for the base class. It initializes everything to false
InputState::InputState()
    : down( false )
    , justPressed( false )
    , justReleased( false )
{

}


// Updates the state assuming the input is currently pressed
void InputState::InputIsDown()
{
    // If isDown is false and the input is pressed then this is
    // the first time it's been updated since the input was pressed
    if( down == false ) {
        justPressed = true;
    } else {
        justPressed = false;
    }

    down = true;
    justReleased = false;
}

// Updates the state assuming the input is not currently pressed
void InputState::InputIsNotDown()
{
    // If isDown is true and the input is not pressed then this is
    // the first time it's been updated since the input was released
    if( down == true ) {
        justReleased = true;
    } else {
        justReleased = false;
    }

    down = false;
    justPressed = false;
}


/*******************************************************
 *                     Mouse Input                     *
 *******************************************************/

// Create the static member
sf::Vector2i MouseState::position;

// The constructor for the child class that handles mouse input.
// The only parameter is the button to be associated with the object
MouseState::MouseState( const sf::Mouse::Button& b )
    : button( b )
{

}

// This updates the state of the input based on the previous
// state of the class and the current state of the input
void MouseState::Update()
{
    if( sf::Mouse::isButtonPressed( button ) ) {
        InputState::InputIsDown();
    } else {
        InputState::InputIsNotDown();
    }
}

// update the location of the cursor
void MouseState::UpdateMouseCoords( sf::Window& window )
{
    // Get the coordinates of the cursor relative to the window
    MouseState::position = sf::Mouse::getPosition( window );
}

// Two MouseStates are equal if they monitor the same button
bool operator==( const MouseState& A, const MouseState& B )
{
    return A.button == B.button;
}

/*******************************************************
 *                  Keyboard Input                     *
 *******************************************************/

// The constructor for the child class that handles mouse input.
// The only parameter is the button to be associated with the object
KeyboardState::KeyboardState( const sf::Keyboard::Key& k )
    : key( k )
{

}

// This updates the state of the input based on the previous
// state of the class and the current state of the input
void KeyboardState::Update()
{
    if( sf::Keyboard::isKeyPressed( key ) ) {
        InputState::InputIsDown();
    } else {
        InputState::InputIsNotDown();
    }
}

// Two KeyboardState are equal if they monitor the same key
bool operator==( const KeyboardState& A, const KeyboardState& B )
{
    return A.key == B.key;
}
