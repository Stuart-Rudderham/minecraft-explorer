/*
 * This file implements the manager that
 * handles monitoring input from the user
 *
 * Author:      Stuart Rudderham
 * Created:     November 3, 2012
 * Last Edited: May 5, 2013
 */

#include "Input/InputManager.hpp"
#include "Input/Input.hpp"

// ctor
InputManager::InputManager()
{

}

// dtor. Deletes all the monitored inputs
InputManager::~InputManager()
{
    for( KeyboardState* key : keysMonitored ) {
        delete key;
    }

    for( MouseState* button : buttonsMonitored ) {
        delete button;
    }
}

// Update all the monitored inputs
void InputManager::UpdateInputs( sf::Window& window )
{
    // Update the location of the mouse in game coords
    MouseState::UpdateMouseCoords( window );

    // Update all the inputs we're listening for
    for( KeyboardState* key : keysMonitored ) {
        key->Update();
    }

    for( MouseState* button : buttonsMonitored ) {
        button->Update();
    }
}

const KeyboardState* InputManager::ListenFor( sf::Keyboard::Key key )
{
    // Add a new keyboard key to listen for
    KeyboardState* listener = new KeyboardState( key );
    keysMonitored.push_back( listener );

    // Make sure it's unique
    dbAssert( UniqueInputs(), "Duplicate key ", key );

    // Return a const reference so the caller can
    // monitor the state of the key but not update it
    return listener;
}

const MouseState* InputManager::ListenFor( sf::Mouse::Button button )
{
    // Add a new keyboard key to listen for
    MouseState* listener = new MouseState( button );
    buttonsMonitored.push_back( listener );

    // Make sure it's unique
    dbAssert( UniqueInputs(), "Duplicate button ", button );

    // Return a const reference so the caller can
    // monitor the state of the key but not update it
    return listener;
}

bool InputManager::UniqueInputs()
{
    // Make sure each keyboard key monitored is unique
    for( auto i = keysMonitored.begin(); i != keysMonitored.end(); ++i ) {
        for( auto j = i + 1; j != keysMonitored.end(); ++j ) {
            if( (**i) == (**j) ) {
                return false;
            }
        }
    }

    // Make sure each mouse button monitored is unique
    for( auto i = buttonsMonitored.begin(); i != buttonsMonitored.end(); ++i ) {
        for( auto j = i + 1; j != buttonsMonitored.end(); ++j ) {
            if( (**i) == (**j) ) {
                return false;
            }
        }
    }

    return true;
}
