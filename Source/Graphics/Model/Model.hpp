/*
 * This header defines a simple model class
 * that is pretty much just a wrapper around
 * VBOs and VAOs. It does not provide any
 * functions for loading models from a file
 *
 * Author:      Stuart Rudderham
 * Created:     June 2, 2013
 * Last Edited: August 4, 2013
 */

#ifndef MODEL_HPP_INCLUDED
#define MODEL_HPP_INCLUDED

#include "Graphics/OpenGL/VertexArrayObject/VertexArrayObject.hpp"
#include "Graphics/OpenGL/VertexBufferObject/VertexBufferObject.hpp"
#include <vector>

template<typename VertexType>
class Model {
    public:
        Model();
        ~Model() = default;

        // FIXME: I feel like these should have very
        // similar function prototypes, but they
        // don't (i.e. const vs non-const, reference
        // vs rvalue-reference)
        template<typename... Args>                  // Adds a new vertex to the buffer, constructing it in-place so
        void AddVertex( Args&&... args );           // that no extra copies are made.

        template<typename... Args>                  // Add a quad made out of two triangles (4 unique vertices, but
        void AddQuad( const Args(&...args)[4] );    // 6 added in total) to the buffer. The vertices are constructed
                                                    // in-place so that no extra copies are made.

        void DefineVAOLayout();                     // Should be specialized manually for each VertexType.

        void CopyToVBO( GLenum usage );             // Copy data from the CPU buffer into the VBO, then clear the CPU buffer.

        void Draw() const;                          // Draw all the triangles in the VBO.

        void SetExpectedVertexNum( int n );         // Make the CPU buffer bigger so that there are less reallocations.
        void SetExpectedQuadNum  ( int n );         // Should be called before adding any verticies.

        int GetNumVertices() const;                 // Getters.
        int GetVBOSize() const;

    private:
        std::vector<VertexType> cpuBuffer;          // A contiguous buffer in CPU RAM that holds all the vertex data for the Model.

        int numVertices;                            // The number of verticies in the VBO. Needed because we clear the CPU buffer
                                                    // and thus cannot check its size.

        VertexBufferObject VBO;                     // The VBO for the Model.
        VertexArrayObject  VAO;                     // The VAO for the Model.

        void SetAttribute(                          // A wrapper around glVertexAttribPointer.
            GLuint index,
            GLint size,
            GLenum type,
            GLboolean normalized,
            GLsizei stride,
            const GLvoid* offset
        );
};

#include "Graphics/Model/Model.inl"

#endif // MODEL_HPP_INCLUDED
