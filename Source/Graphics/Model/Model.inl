/*
 * This file implements a simple model class
 * that is pretty much just a wrapper around
 * VBOs and VAOs. It does not provide any
 * functions for loading models from a file
 *
 * Author:      Stuart Rudderham
 * Created:     June 2, 2013
 * Last Edited: August 4, 2013
 */

#include "DebugTools/Debug.hpp"

// ctor
template<typename VertexType>
Model<VertexType>::Model()
{
    // Need to be standard-layout for offsetof() macro
    static_assert( std::is_standard_layout<VertexType>::value == true, "Can't use offsetof()" );

    // Define the layout of the VAO
    VBO.Bind();
    VAO.Bind();

    DefineVAOLayout();

    VBO.Unbind();
    VAO.Unbind();
}

// Adds a new vertex to the buffer, constructing it in-place so that no extra copies are made
template<typename VertexType> template<typename... Args>
void Model<VertexType>::AddVertex( Args&&... args )
{
    cpuBuffer.emplace_back( args... );
}

// Emulate the functionality of the (removed) GL_QUADS drawing mode
// The 4 verticies are assumed to be coplanar and given in a counter-clockwise order
// The quad is split into two triangles as shown below
//
// 0----3
// |   /|
// |  / |
// | /  |
// |/   |
// 1----2
//
// The created triangles are: 0-1-3 and 1-2-3
//
// Note: the created triangles may not exactly reflect the diagram depending on the
// order the verticies are given in the array, but so long they have a counter-clockwise
// ordering the result will be correct
//
// The syntax for this function is kind of gross. What it does is
// take in a variable number of 4 element arrays. It uses the data
// in these arrays to construct each vertex in-place in the buffer.
// This avoid constructing each vertex and then copying it into the
// buffer, so we save on one call to the vertex copy constructor for
// each vertex added.
//
// Since we use variadic templates it is very flexible. If we decide
// to add another attribute to any of the Vertex classes (e.g. a normal)
// all that has to be done pass 3 more arrays (one for each component of
// the normal) to the AddQuad function and they will automatically
// get forwarded to the Vertex constructor and handled there.
template<typename VertexType> template<typename... Args>
void Model<VertexType>::AddQuad( const Args(&...args)[4] )
{
    // Triangle 1
    AddVertex( args[0]... );
    AddVertex( args[1]... );
    AddVertex( args[3]... );

    // Triangle 2
    AddVertex( args[1]... );
    AddVertex( args[2]... );
    AddVertex( args[3]... );
}

// A wrapper around glVertexAttribPointer
template<typename VertexType>
void Model<VertexType>::SetAttribute( GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* offset )
{
    dbAssert( VAO.IsActive(), "Setting attribute on wrong VAO" );
    dbAssert( VBO.IsActive(), "VAO associated with wrong VBO" );

    glEnableVertexAttribArray(index);
    glVertexAttribPointer(
        index,
        size,
        type,
        normalized,
        stride,
        offset
    );
}

template<typename VertexType>
void Model<VertexType>::CopyToVBO( GLenum usage )
{
    VBO.Bind();

    glBufferData( GL_ARRAY_BUFFER, cpuBuffer.size() * sizeof(VertexType), cpuBuffer.data(), usage );

    numVertices = cpuBuffer.size();

    // Don't need it in CPU memory anymore
    cpuBuffer.clear();
    cpuBuffer.shrink_to_fit();

    VBO.Unbind();
}

// Draw all the triangles in the Model
template<typename VertexType>
void Model<VertexType>::Draw() const
{
    VAO.Bind();
    glDrawArrays( GL_TRIANGLES, 0, numVertices );
    VAO.Unbind();
}

template<typename VertexType>
void Model<VertexType>::SetExpectedVertexNum( int n )
{
    cpuBuffer.reserve( n );
}

template<typename VertexType>
void Model<VertexType>::SetExpectedQuadNum( int n )
{
    SetExpectedVertexNum( n * 6 ); // 1 quad -> 2 triangles -> 6 verticies
}

template<typename VertexType>
int Model<VertexType>::GetNumVertices() const
{
    return numVertices;
}

template<typename VertexType>
int Model<VertexType>::GetVBOSize() const
{
    return GetNumVertices() * sizeof(VertexType);
}
