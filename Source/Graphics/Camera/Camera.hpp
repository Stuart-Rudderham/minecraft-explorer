/*
 * This header defines a Camera class
 *
 * Author:      Stuart Rudderham
 * Created:     August 24, 2013
 * Last Edited: August 24, 2013
 */

#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include "World/CoordinateSystem.hpp"
#include <glm/glm.hpp>

// FIXME: We should really have a PerspectiveCamera and an OrthographicCamera
// rather than cramming the functionality of both into one class.

class Camera {
    public:
        // ctor. Creates a Camera at (0, 0, 0) looking in the direction (0, 0, -1)
        // with the given projection.
        Camera( float fovY,
                float aspectRatio,
                float nearClipPlane,
                float farClipPlane );

        Camera( float leftClipPlane,
                float rightClipPlane,
                float bottomClipPlane,
                float topClipPlane,
                float nearClipPlane,
                float farClipPlane );

        // Getter for the View-Projection matrix.
        // Will recalculate if necessary.
        const glm::mat4& GetViewProjectionMatrix() const;

        // Setters for the projection matrix
        void SetPerspectiveProjection( float fovY,
                                       float aspectRatio,
                                       float nearClipPlane,
                                       float farClipPlane );

        void SetFOV( float fovY );
        void SetAspectRatio( float aspectRatio );

        void SetOrthographicProjection( float leftClipPlane,
                                        float rightClipPlane,
                                        float bottomClipPlane,
                                        float topClipPlane,
                                        float nearClipPlane,
                                        float farClipPlane );

        void SetScreenSize( float width, float height );

        // Setters for the view matrix
        void MoveTo( glm::vec3 newPos );
        void MoveBy( glm::vec3 delta );
        void SetViewDirection( glm::vec3 newDir );

        bool IsAABBInView( glm::vec3 topLeftFront, glm::vec3 bottomRightBack ) const;

    private:
        Camera();                                               // Private ctor that sets everything except the projection

        // Values used to create a 3D perspective projection
        // Descriptions are taken from the below link
        //     http://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml
        struct PerspectiveProjection {
            float fovY;                                         // Specifies the field of view angle, in degrees, in the y direction.
            float aspectRatio;                                  // Specifies the aspect ratio that determines the field of view in the x direction.
                                                                // The aspect ratio is the ratio of x (width) to y (height).
            float nearClipPlane;                                // Specifies the distance from the viewer to the near clipping plane (always positive).
            float farClipPlane;                                 // Specifies the distance from the viewer to the far clipping plane (always positive).
        };

        // Values used to create a orthographic projection
        // Descriptions are taken from the below link
        //     http://www.opengl.org/sdk/docs/man2/xhtml/glOrtho.xml
        struct OrthographicProjection {
            float leftClipPlane;                                // Specify the coordinates for the left and right vertical clipping planes.
            float rightClipPlane;

            float bottomClipPlane;                              // Specify the coordinates for the bottom and top horizontal clipping planes.
            float topClipPlane;

            float nearClipPlane;                                // Specify the distances to the nearer and farther depth clipping planes.
            float farClipPlane;                                 // These values are negative if the plane is to be behind the viewer.
        };

        // The Camera will either have a perspective or orthographic projection
        union Projection {
            PerspectiveProjection  perspective;
            OrthographicProjection orthographic;
        } projection;

        // Keep track of which projection is currently being used
        enum class ProjectionType {
            Perspective,
            Orthographic,
        } projectionType;

        // Values used to create the view matrix
        WorldCoord position;                                    // The Camera's position

        glm::vec3 viewDirection;                                // The direction the Camera is looking

        float zoom;                                             // The zoom level of the Camera

        // The resulting View-Perspective matrix.
        mutable glm::mat4 VPMatrix;                             // This is mutable because we lazily update it on demand
        mutable bool shouldRecalculate;                         // A dirty bit that is set if we need to recalculate the matrix

        inline void SetDirty() {
            shouldRecalculate = true;
        }
};

#endif // CAMERA_HPP_INCLUDED
