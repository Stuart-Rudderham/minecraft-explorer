/*
 * This file implements a Camera class
 *
 * Author:      Stuart Rudderham
 * Created:     August 24, 2013
 * Last Edited: August 24, 2013
 */

#include "Graphics/Camera/Camera.hpp"
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>

#ifndef M_PI
    #define M_PI    3.14159265358979323846f
#endif

/*
 * Private constructor that sets everything except the projection.
 */
Camera::Camera()
    : position( 0.0f, 0.0f, 0.0f )
    , viewDirection( 0.0f, 0.0f, -1.0f )
    , zoom( 1.0f )
    , VPMatrix( 1.0f )
    , shouldRecalculate( false )
{

}

/*
 * Constructor for perspective projection
 */
Camera::Camera( float fovY, float aspectRatio, float nearClipPlane, float farClipPlane )
    : Camera()
{
    SetPerspectiveProjection(
        fovY,
        aspectRatio,
        nearClipPlane,
        farClipPlane
    );
}

Camera::Camera( float leftClipPlane, float rightClipPlane, float bottomClipPlane, float topClipPlane, float nearClipPlane, float farClipPlane )
    : Camera()
{
    SetOrthographicProjection(
        leftClipPlane,
        rightClipPlane,
        bottomClipPlane,
        topClipPlane,
        nearClipPlane,
        farClipPlane
    );
}

/*
 * Getter for the View-Projection matrix. Will recalculate if necessary.
 */
const glm::mat4& Camera::GetViewProjectionMatrix() const
{
    if( shouldRecalculate ) {
        glm::mat4 projMat( 1.0f );
        glm::mat4 viewMat( 1.0f );

        // Recalculate the Projection matrix
        switch( projectionType ) {
            case ProjectionType::Perspective:
                projMat = glm::perspective(
                    projection.perspective.fovY,
                    projection.perspective.aspectRatio,
                    projection.perspective.nearClipPlane,
                    projection.perspective.farClipPlane
                );
                break;
            case ProjectionType::Orthographic:
                projMat = glm::ortho(
                    projection.orthographic.leftClipPlane,
                    projection.orthographic.rightClipPlane,
                    projection.orthographic.bottomClipPlane,
                    projection.orthographic.topClipPlane,
                    projection.orthographic.nearClipPlane,
                    projection.orthographic.farClipPlane
                );
                break;
            default:
                dbCrash("Unknown ProjectionType");
                break;
        }

        // Recalculate the View matrix
        viewMat = glm::lookAt( position, position + viewDirection, glm::vec3(0, 1, 0) );

        // Combine them
        VPMatrix = projMat * viewMat;
        shouldRecalculate = false;
    }

    return VPMatrix;
}

/*
 * Setters for the projection matrix
 */
void Camera::SetPerspectiveProjection( float fovY, float aspectRatio, float nearClipPlane, float farClipPlane )
{
    projection.perspective.fovY          = fovY;
    projection.perspective.aspectRatio   = aspectRatio;
    projection.perspective.nearClipPlane = nearClipPlane;
    projection.perspective.farClipPlane  = farClipPlane;

    projectionType = ProjectionType::Perspective;
    SetDirty();
}

void Camera::SetFOV( float fovY )
{
    projection.perspective.fovY = fovY;

    projectionType = ProjectionType::Perspective;
    SetDirty();
}

void Camera::SetAspectRatio( float aspectRatio )
{
    projection.perspective.aspectRatio = aspectRatio;

    projectionType = ProjectionType::Perspective;
    SetDirty();
}

void Camera::SetOrthographicProjection( float leftClipPlane, float rightClipPlane, float bottomClipPlane, float topClipPlane, float nearClipPlane, float farClipPlane )
{
    projection.orthographic.leftClipPlane   = leftClipPlane;
    projection.orthographic.rightClipPlane  = rightClipPlane;
    projection.orthographic.bottomClipPlane = bottomClipPlane;
    projection.orthographic.topClipPlane    = topClipPlane;
    projection.orthographic.nearClipPlane   = nearClipPlane;
    projection.orthographic.farClipPlane    = farClipPlane;

    projectionType = ProjectionType::Orthographic;
    SetDirty();
}

void Camera::SetScreenSize( float width, float height )
{
    projection.orthographic.rightClipPlane  = width;
    projection.orthographic.bottomClipPlane = height;

    projectionType = ProjectionType::Orthographic;
    SetDirty();
}

/*
 * Setters for the view matrix
 */
void Camera::MoveTo( glm::vec3 newPos ) {
    position = newPos;
    SetDirty();
}

void Camera::MoveBy( glm::vec3 delta ) {
    position = position + delta;
    SetDirty();
}

void Camera::SetViewDirection( glm::vec3 newDir )
{
    viewDirection = newDir;
    SetDirty();
}

bool Camera::IsAABBInView( glm::vec3 minCorner, glm::vec3 maxCorner ) const
{
    // FIXME: This is not quite correct yet, if the camera is pointing straight up
    // then everything should be culled but it isn't. So disable the feature for now
    return true;

    // http://zeuxcg.org/2009/01/31/view-frustum-culling-optimization-introduction/

    const glm::mat4& m = GetViewProjectionMatrix();

    glm::vec4 planes[] =
    {
        glm::vec4( m[0][3] + m[0][0], m[1][3] + m[1][0], m[2][3] + m[2][0], m[3][3] + m[3][0] ),    // Right
        glm::vec4( m[0][3] - m[0][0], m[1][3] - m[1][0], m[2][3] - m[2][0], m[3][3] - m[3][0] ),    // Left
        glm::vec4( m[0][3] - m[0][1], m[1][3] - m[1][1], m[2][3] - m[2][1], m[3][3] - m[3][1] ),    // Top
        glm::vec4( m[0][3] + m[0][1], m[1][3] + m[1][1], m[2][3] + m[2][1], m[3][3] + m[3][1] ),    // Bottom
        glm::vec4( m[0][3] - m[0][2], m[1][3] - m[1][2], m[2][3] - m[2][2], m[3][3] - m[3][2] ),    // Far
        glm::vec4( m[0][2]          , m[1][2]          , m[2][2]          , m[3][2]           ),    // Near
    };

    // Points are already in world space.
    glm::vec4 points[8] =
    {
        glm::vec4( minCorner.x, minCorner.y, minCorner.z, 1.0f ),
        glm::vec4( minCorner.x, minCorner.y, maxCorner.z, 1.0f ),
        glm::vec4( minCorner.x, maxCorner.y, minCorner.z, 1.0f ),
        glm::vec4( minCorner.x, maxCorner.y, maxCorner.z, 1.0f ),
        glm::vec4( maxCorner.x, minCorner.y, minCorner.z, 1.0f ),
        glm::vec4( maxCorner.x, minCorner.y, maxCorner.z, 1.0f ),
        glm::vec4( maxCorner.x, maxCorner.y, minCorner.z, 1.0f ),
        glm::vec4( maxCorner.x, maxCorner.y, maxCorner.z, 1.0f ),
    };

    for( const auto& plane : planes )
    {
        bool inside = false;

        for( const auto& point : points )
        {
            if( glm::dot(point, plane) > 0 )
            {
                inside = true;
                break;
            }
        }

        if (!inside)
        {
            return false;
        }
    }

    return true;
}

#if 0
#include "World/Minecraft/Map.hpp"
#include "DebugTools/Debug.hpp"
#include "Input/InputManager.hpp"
#include "Input/Input.hpp"
#include <SFML/OpenGL.hpp>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>

#ifndef M_PI
    #define M_PI    3.14159265358979323846f
#endif

// ctor
Player::Player()
    : position(0.0f, 0.0f, 0.0f)
    , velocity(0.0f, 0.0f, 0.0f)
    , pitchRotation(0.0f)
    , yawRotation(0.0f)
    , zoom(1.0f)
    , viewMatrix( 1.0f )
    , forwardKey ( InputManager::Get().ListenFor( sf::Keyboard::W      ) )
    , backwardKey( InputManager::Get().ListenFor( sf::Keyboard::S      ) )
    , leftKey    ( InputManager::Get().ListenFor( sf::Keyboard::A      ) )
    , rightKey   ( InputManager::Get().ListenFor( sf::Keyboard::D      ) )
    , upKey      ( InputManager::Get().ListenFor( sf::Keyboard::Space  ) )
    , downKey    ( InputManager::Get().ListenFor( sf::Keyboard::LShift ) )
{

}

// Set the Player's forward/backward velocity
// Based on the view direction
void Player::SetForwardVelocity( float speed )
{
    velocity.x +=  speed * sinf(yawRotation * M_PI / 180.0f);
    velocity.z += -speed * cosf(yawRotation * M_PI / 180.0f);
}

// Set the Player's left/right velocity
// Based on the horizontal vector perpendicular to the view direction
void Player::SetStrafeVelocity( float speed )
{
    velocity.x += speed * cosf(yawRotation * M_PI / 180.0f);
    velocity.z += speed * sinf(yawRotation * M_PI / 180.0f);
}

// Set the Player's up/down velocity
// Is easy because it doesn't depend on view direction
void Player::SetVerticalVelocity( float speed )
{
    velocity.y += speed;
}

void Player::Update( float deltaT, glm::vec2 mouseDelta, Map& map )
{
    const float speed = 5;
    const float lookSensitivity = 10.0f;
    const float friction = 0.75f;

    // Move the view direction based on mouse movement
    LookLeftRight( (float)mouseDelta.x / lookSensitivity );
    LookUpDown   ( (float)mouseDelta.y / lookSensitivity );

    // Forward movement
    if( forwardKey->IsDown() ) {
        SetForwardVelocity( speed );
    } else if( backwardKey->IsDown() ) {
        SetForwardVelocity( -speed );
    }

    // Strafe movement
    if( leftKey->IsDown() ) {
        SetStrafeVelocity( -speed );
    } else if( rightKey->IsDown() ) {
        SetStrafeVelocity( speed );
    }

    // Vertical movement
    if( upKey->IsDown() ) {
        SetVerticalVelocity( speed );
    } else if( downKey->IsDown() ) {
        SetVerticalVelocity( -speed );
    }

    // Update the position and velocity
    if( map.Colliding( position + velocity * deltaT ) ) {
        velocity = glm::vec3(0, 0, 0);
    }
    position += velocity * deltaT;
    velocity = velocity * friction;

    // Update the view matrix
    viewMatrix = glm::mat4( 1.0f );

    viewMatrix = glm::rotate( viewMatrix, pitchRotation, glm::vec3(1.0f, 0.0f, 0.0f) );     // rotate around x-axis
    viewMatrix = glm::rotate( viewMatrix, yawRotation  , glm::vec3(0.0f, 1.0f, 0.0f) );     // rotate around y-axis

    viewMatrix = glm::translate( viewMatrix, -position );       // negative translation because we want the camera
                                                                // to be at (0, 0, 0)

    float FOV = 40.0f;
    float aspectRatio = 960.0f / 540.0f;
    float nearClipPlane = 0.01;
    float farClipPlane = 1000;

    // Setup matrices
    glm::mat4 ProjMat = glm::perspective( FOV, aspectRatio, nearClipPlane, farClipPlane );

    viewMatrix = ProjMat * viewMatrix;

    glm::vec4 viewDir = viewMatrix * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);

    dbPrintToScreen( "Global Pos   : ", position                );
    dbPrintToScreen( "Global Block : ", position.ToBlock()      );
    dbPrintToScreen( "Region       : ", position.ToRegion()     );
    dbPrintToScreen( "Chunk        : ", position.ToChunk()      );
    dbPrintToScreen( "Local Block  : ", position.ToLocalBlock() );

    dbPrintToScreen( "View  : ", viewDir );
}
#endif
