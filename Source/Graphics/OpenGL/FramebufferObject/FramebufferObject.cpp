/*
 * This file implements a simple wrapper around
 * an OpenGL Framebuffer Object (FBO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 3, 2014
 * Last Edited: August 3, 2014
 */

#include "Graphics/OpenGL/FrameBufferObject/FrameBufferObject.hpp"

template<>
void GenericOpenGLObject<FrameBufferObject>::InternalCreate()
{
    glGenFramebuffers( 1, &objectID );
}

template<>
void GenericOpenGLObject<FrameBufferObject>::InternalDestroy() {
    glDeleteFramebuffers( 1, &objectID );
}

template<>
void GenericOpenGLObject<FrameBufferObject>::InternalBind() const
{
    glBindFramebuffer( GL_FRAMEBUFFER, objectID );
}

template<>
void GenericOpenGLObject<FrameBufferObject>::InternalUnbind()
{
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}
