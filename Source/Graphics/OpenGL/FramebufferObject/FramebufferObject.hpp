/*
 * This header defines a simple wrapper around
 * an OpenGL Framebuffer Object (FBO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 3, 2014
 * Last Edited: August 3, 2014
 */

#ifndef FRAMEBUFFEROBJECT_HPP_INCLUDED
#define FRAMEBUFFEROBJECT_HPP_INCLUDED

#include "Graphics/OpenGL/GenericOpenGLObject.hpp"

class FrameBufferObject : public GenericOpenGLObject<FrameBufferObject> {
    public:

    private:
};

#endif // FRAMEBUFFEROBJECT_HPP_INCLUDED


