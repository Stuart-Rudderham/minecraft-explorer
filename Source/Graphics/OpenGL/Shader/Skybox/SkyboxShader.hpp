/*
 * This header defines the shader
 * used to draw the Skybox and
 * all the objects in it
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: August 10, 2013
 */

#ifndef TESTSHADER_HPP_INCLUDED
#define TESTSHADER_HPP_INCLUDED

#include "Graphics/OpenGL/Shader/Shader.hpp"
#include <glm/glm.hpp>

class SkyboxShader : public Shader {
    public:
        SkyboxShader( const char* vertexShaderFile, const char* fragmentShaderFile );

        void SetMVPMatrix( glm::mat4x4& MVP );
        void SetColour( glm::vec3 tint );

    private:
        GLint matrixID;
        GLint tintID;
};

#endif // TESTSHADER_HPP_INCLUDED
