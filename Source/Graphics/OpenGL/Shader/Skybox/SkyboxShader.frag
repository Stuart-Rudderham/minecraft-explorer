#version 330 core

uniform sampler2D tex;              // The provided texture
uniform vec3 tint;                  // A colour to tint the texture

in vec2 UV;                         // The texture coordinates received by the vertex shader

out vec4 outputColor;               // The resulting output colour



void main() {
    outputColor = texture( tex, UV ) * vec4(tint, 1);
}
