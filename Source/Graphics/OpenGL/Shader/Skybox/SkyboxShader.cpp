/*
 * This file implements the shader
 * used to draw the Skybox and
 * all the objects in it
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2013
 * Last Edited: August 10, 2013
 */

#include "Graphics/OpenGL/Shader/Skybox/SkyboxShader.hpp"
#include "DebugTools/Debug.hpp"

// ctor.
SkyboxShader::SkyboxShader( const char* vertexShaderFile, const char* fragmentShaderFile )
    : Shader( vertexShaderFile, fragmentShaderFile )
    , matrixID( -1 )
    , tintID( -1 )
{
    matrixID = GetUniformLocation( "MVP"  );
    tintID   = GetUniformLocation( "tint" );
}

void SkyboxShader::SetMVPMatrix( glm::mat4x4& MVP )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniformMatrix4fv( matrixID, 1, GL_FALSE, &MVP[0][0] );
}

void SkyboxShader::SetColour( glm::vec3 tint )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniform3f( tintID, tint.x, tint.y, tint.z );
}
