/*
 * This file implementsthe shader
 * used to draw all the Blocks in
 * a Chunk.
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: August 10, 2013
 */

#include "Graphics/OpenGL/Shader/Block/BlockShader.hpp"
#include "DebugTools/Debug.hpp"

// ctor.
BlockShader::BlockShader( const char* vertexShaderFile, const char* fragmentShaderFile )
    : Shader( vertexShaderFile, fragmentShaderFile )
    , matrixID( -1 )
    , alphaID( -1 )
    , sunlightDirID( -1 )
    , sunlightIntensityID( -1 )
{
    matrixID            = GetUniformLocation( "MVP"            );
    alphaID             = GetUniformLocation( "alpha"          );
    sunlightDirID       = GetUniformLocation( "lightDir"       );
    sunlightIntensityID = GetUniformLocation( "lightIntensity" );
}

void BlockShader::SetMVPMatrix( glm::mat4x4& MVP )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniformMatrix4fv( matrixID, 1, GL_FALSE, &MVP[0][0] );
}

void BlockShader::SetAlpha( float alpha )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniform1f( alphaID, alpha );
}

void BlockShader::SetSunlightIntensity( float intensity )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniform1f( sunlightIntensityID, intensity );
}

void BlockShader::SetSunlightDirection( glm::vec3 lightDir )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniform3f( sunlightDirID, lightDir.x, lightDir.y, lightDir.z );
}
