#version 330 core

uniform sampler2D tex;                  // The provided texture
uniform float alpha;                    // The transparancy for the Block. Can stack with texture transparency
uniform float lightIntensity;           // How strong the ambient light is

in vec2 UV;                             // The texture coordinates received from the vertex shader
in float diffuseCoefficient;            // The diffuse lighting calculation received from the vertex shader

out vec4 outputColor;                   // The resulting output colour



void main() {
    vec4 surfaceColor = texture( tex, UV ) * vec4(1, 1, 1, alpha);

    // Right now light is always white 
    vec4 ambientLight =                      0.25           * surfaceColor;
    vec4 diffuseLight = diffuseCoefficient * lightIntensity * surfaceColor;

    outputColor = vec4( ambientLight.rgb + diffuseLight.rgb, surfaceColor.a );
}
