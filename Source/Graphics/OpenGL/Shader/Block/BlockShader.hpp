/*
 * This header defines the shader
 * used to draw all the Blocks in
 * a Chunk.
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: August 10, 2013
 */

#ifndef BLOCKSHADER_HPP_INCLUDED
#define BLOCKSHADER_HPP_INCLUDED

#include "Graphics/OpenGL/Shader/Shader.hpp"
#include <glm/glm.hpp>

class BlockShader : public Shader {
    public:
        BlockShader( const char* vertexShaderFile, const char* fragmentShaderFile );

        void SetMVPMatrix( glm::mat4x4& MVP );
        void SetAlpha( float alpha );
        void SetSunlightIntensity( float intensity );
        void SetSunlightDirection( glm::vec3 lightDir );

    private:
        GLint matrixID;
        GLint alphaID;
        GLint sunlightDirID;
        GLint sunlightIntensityID;
};

#endif // BLOCKSHADER_HPP_INCLUDED
