#version 330 core

uniform mat4 MVP;                           // The Model-View-Projection matrix 
uniform vec3 lightDir;

layout(location = 0) in vec3 vertPos;       // The vertex position (in model coordinates)
layout(location = 1) in vec2 vertUV;        // The vertex texture coordinates
layout(location = 2) in vec3 vertNorm;      // The vertex normal

out vec2 UV;                                // The texture coordinates to be passed through to the fragment shader 
out float diffuseCoefficient;               // The diffuse lighting calculation to be passed through to the fragment shader 


void main(){
    // Need to operate on 4D homogeneous vector
    vec4 v = vec4( vertPos, 1.0 ); 

    // Output position of the vertex, in clip space
    gl_Position = MVP * v;

    // Forward the texture coordinates through
    UV = vertUV;

    // Since the model transformation for Chunks doesn't scale or
    // rotate we don't have to transform the normals at all
    diffuseCoefficient = clamp( dot( lightDir, vertNorm ), 0, 1 );
}