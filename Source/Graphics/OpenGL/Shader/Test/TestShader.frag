#version 330 core

uniform sampler2D tex;                  // The provided texture

in vec2 UV;                             // The texture coordinates received by the vertex shader
in vec3 tintColour;                     // The tint-colour received by the vertex shader

out vec4 outputColor;                   // The resulting output colour



void main() {
    //outputColor = vec4( tintColour, 1.0 );
    outputColor = texture( tex, UV );
}




