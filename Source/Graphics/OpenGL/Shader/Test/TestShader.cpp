/*
 * This file implements a random test
 * shader.
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: May 12, 2013
 */

#include "Graphics/OpenGL/Shader/Test/TestShader.hpp"
#include "DebugTools/Debug.hpp"

// ctor.
TestShader::TestShader( const char* vertexShaderFile, const char* fragmentShaderFile )
    : Shader( vertexShaderFile, fragmentShaderFile )
    , matrixID( -1 )
{
    matrixID = GetUniformLocation( "MVP" );
}

void TestShader::SetMVPMatrix( glm::mat4x4& MVP )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniformMatrix4fv( matrixID, 1, GL_FALSE, &MVP[0][0] );
}
