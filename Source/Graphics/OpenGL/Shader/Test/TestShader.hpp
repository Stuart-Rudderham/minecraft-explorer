/*
 * This header defines a random test
 * shader.
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: May 12, 2013
 */

#ifndef TESTSHADER_HPP_INCLUDED
#define TESTSHADER_HPP_INCLUDED

#include "Graphics/OpenGL/Shader/Shader.hpp"
#include <glm/glm.hpp>

class TestShader : public Shader {
    public:
        TestShader( const char* vertexShaderFile, const char* fragmentShaderFile );

        void SetMVPMatrix( glm::mat4x4& MVP );

    private:
        GLint matrixID;
};

#endif // TESTSHADER_HPP_INCLUDED
