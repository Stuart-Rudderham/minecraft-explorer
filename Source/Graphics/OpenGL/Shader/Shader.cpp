/*
 * This file implements a wrapper around
 * an OpenGL vertex shader and fragment
 * shader. It provides functionality only
 * for loading/compiling/linking, as well
 * as activating the shader
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: August 10, 2013
 */

#include "Graphics/OpenGL/Shader/Shader.hpp"
#include "DebugTools/Debug.hpp"
#include <string>
#include <fstream>
#include <vector>

template<> void GenericOpenGLObject<Shader>::InternalCreate ()       { objectID = glCreateProgram(); }
template<> void GenericOpenGLObject<Shader>::InternalDestroy()       { glDeleteProgram( objectID ); }
template<> void GenericOpenGLObject<Shader>::InternalBind   () const { glUseProgram( objectID ); }
template<> void GenericOpenGLObject<Shader>::InternalUnbind ()       { glUseProgram( 0 ); }

static void CompileShader( GLuint shaderID, const char* shaderFilename )
{
    // Open the file
    std::ifstream shaderFile( shaderFilename );
    dbErrorCheck( shaderFile.is_open(), "Unable to open ", shaderFilename );

    // Read shader code into memory
    std::string shaderCode;
    std::string line = "";

    while( getline(shaderFile, line) ) {
        shaderCode += "\n" + line;
    }

    // Compile shader
    dbPrintToConsole( "Compiling ", shaderFilename );

    const char* shaderCodeCString = shaderCode.c_str();
    glShaderSource(shaderID, 1, &shaderCodeCString, NULL);
    glCompileShader(shaderID);

    // Check for any errors
    GLint result = GL_FALSE;
    glGetShaderiv( shaderID, GL_COMPILE_STATUS, &result );

    if( result == false ) {
        int errorMessageLength = 0;
        glGetShaderiv( shaderID, GL_INFO_LOG_LENGTH, &errorMessageLength );

        std::vector<char> errorMessage( errorMessageLength );
        glGetShaderInfoLog( shaderID, errorMessageLength, NULL, &errorMessage.front() );

        dbPrintToConsole( "", &errorMessage.front() );
        dbCrash( "Shader compile error" );
    }
}

// ctor. Loads/compiles/links the vertex and fragment shaders
// Code is from here -> http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
Shader::Shader( const char* vertexShaderFile, const char* fragmentShaderFile )
{
    dbPrintToConsole("");

    // Create the shaders
    GLuint vertexShaderID   = glCreateShader(GL_VERTEX_SHADER  );
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Compile them
    CompileShader( vertexShaderID, vertexShaderFile );
    CompileShader( fragmentShaderID, fragmentShaderFile );

    // Link them together
    dbPrintToConsole( "Linking shader" );
    glAttachShader(objectID, vertexShaderID);
    glAttachShader(objectID, fragmentShaderID);
    glLinkProgram(objectID);

    // Check for any linker errors
    GLint result = GL_FALSE;
    glGetProgramiv( objectID, GL_LINK_STATUS, &result );

    if( result == false ) {
        int errorMessageLength = 0;
        glGetProgramiv( objectID, GL_INFO_LOG_LENGTH, &errorMessageLength );

        std::vector<char> errorMessage( errorMessageLength );
        glGetProgramInfoLog( objectID, errorMessageLength, NULL, &errorMessage.front() );

        dbPrintToConsole( "", &errorMessage.front() );
        dbCrash( "Shader link error" );
    }

    // Don't need these anymore
    glDetachShader(objectID, vertexShaderID);
    glDeleteShader(vertexShaderID);

    glDetachShader(objectID, fragmentShaderID);
    glDeleteShader(fragmentShaderID);
}

GLint Shader::GetUniformLocation( const char* variableName )
{
    GLint location = glGetUniformLocation( objectID, variableName );

    dbAssert( location >= 0, "Unable to find uniform variable '", variableName, "'" );

    return location;
}
