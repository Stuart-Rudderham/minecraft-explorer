#version 330 core

uniform mat4 MVP;                           // The Model-View-Projection matrix 

layout(location = 0) in vec2 vertPos;       // The vertex position (in model coordinates)
layout(location = 1) in vec2 vertUV;        // The vertex texture coordinates

out vec2 UV;                                // The texture coordinates to be passed through to the fragment shader 

void main(){
 
    // Need to operate on 4D homogeneous vector
    vec4 v = vec4( vertPos, 0.0, 1.0 ); 

    // Output position of the vertex, in clip space
    gl_Position = MVP * v;

    // Forward the texture coordinates through
    UV         = vertUV;
}
