/*
 * This file implements shader for
 * drawing onscreen text.
 *
 * Author:      Stuart Rudderham
 * Created:     June 2, 2013
 * Last Edited: August 10, 2013
 */

#include "Graphics/OpenGL/Shader/Text/TextShader.hpp"
#include "DebugTools/Debug.hpp"

// ctor.
TextShader::TextShader( const char* vertexShaderFile, const char* fragmentShaderFile )
    : Shader( vertexShaderFile, fragmentShaderFile )
    , matrixID( -1 )
    , textColourID( -1 )
{
    matrixID     = GetUniformLocation( "MVP"         );
    textColourID = GetUniformLocation( "textColour"  );
}

void TextShader::SetMVPMatrix( glm::mat4x4& MVP )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniformMatrix4fv( matrixID, 1, GL_FALSE, &MVP[0][0] );
}

void TextShader::SetTextColour( glm::vec3 textColour )
{
    dbAssert( IsActive(), "Must activate shader before setting uniform variables" );
    glUniform3f( textColourID, textColour.x, textColour.y, textColour.z );
}
