#version 330 core

uniform sampler2D tex;                  // The provided texture
uniform vec3 textColour;                // The colour of the text

in vec2 UV;                             // The texture coordinates received by the vertex shader

out vec4 outputColor;                   // The resulting output colour



void main() {
    outputColor = texture( tex, UV ) * vec4(textColour, 1.0);
}




