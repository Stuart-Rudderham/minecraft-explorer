/*
 * This header defines shader for
 * drawing onscreen text.
 *
 * Author:      Stuart Rudderham
 * Created:     June 2, 2013
 * Last Edited: August 10, 2013
 */

#ifndef TEXTSHADER_HPP_INCLUDED
#define TEXTSHADER_HPP_INCLUDED

#include "Graphics/OpenGL/Shader/Shader.hpp"
#include <glm/glm.hpp>

class TextShader : public Shader {
    public:
        TextShader( const char* vertexShaderFile, const char* fragmentShaderFile );

        void SetMVPMatrix( glm::mat4x4& MVP );
        void SetTextColour( glm::vec3 textColour );

    private:
        GLint matrixID;
        GLint textColourID;
};

#endif // TEXTSHADER_HPP_INCLUDED
