/*
 * This header defines a wrapper around
 * an OpenGL vertex shader and fragment
 * shader. It provides functionality only
 * for loading/compiling/linking, as well
 * as activating the shader
 *
 * Author:      Stuart Rudderham
 * Created:     May 12, 2013
 * Last Edited: August 10, 2013
 */

#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include "Graphics/OpenGL/GenericOpenGLObject.hpp"

class Shader : public GenericOpenGLObject<Shader> {
    public:
        Shader( const char* vertexShaderFile, const char* fragmentShaderFile );

    protected:
        GLint GetUniformLocation( const char* variableName );

    private:
        // Any member variables will be declared in child classes
};


#endif // SHADER_HPP_INCLUDED
