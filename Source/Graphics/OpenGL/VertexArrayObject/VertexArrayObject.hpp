/*
 * This header defines a simple wrapper around
 * an OpenGL Vertex Array Object (VAO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 13, 2013
 */

#ifndef VERTEXARRAYOBJECT_HPP_INCLUDED
#define VERTEXARRAYOBJECT_HPP_INCLUDED

#include "Graphics/OpenGL/GenericOpenGLObject.hpp"

class VertexArrayObject : public GenericOpenGLObject<VertexArrayObject> {
    public:

    private:
};

#endif // VERTEXARRAYOBJECT_HPP_INCLUDED


