/*
 * This file implements a simple wrapper around
 * an OpenGL Vertex Array Object (VAO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 13, 2013
 */

#include "Graphics/OpenGL/VertexArrayObject/VertexArrayObject.hpp"

template<>
void GenericOpenGLObject<VertexArrayObject>::InternalCreate()
{
    glGenVertexArrays( 1, &objectID );
}

template<>
void GenericOpenGLObject<VertexArrayObject>::InternalDestroy() {
    glDeleteVertexArrays( 1, &objectID );
}

template<>
void GenericOpenGLObject<VertexArrayObject>::InternalBind() const
{
    glBindVertexArray( objectID );
}

template<>
void GenericOpenGLObject<VertexArrayObject>::InternalUnbind()
{
    glBindVertexArray( 0 );
}
