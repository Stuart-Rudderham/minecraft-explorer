/*
 * This file implements  a common base
 * for certain OpenGL objects that have
 * similar create/destroy/bind/unbind functions
 * (e.g. VBO, VAO, shaders, etc...). It
 * provided a common framework while the template
 * specializations provide the specific OpenGL
 * functions to call.
 *
 * Author:      Stuart Rudderham
 * Created:     August 7, 2013
 * Last Edited: August 3, 2014
 */

#include "DebugTools/Debug.hpp"

template<typename T>
GenericOpenGLObject<T>::GenericOpenGLObject()
    : objectID( 0 )
{
    InternalCreate();
}

template<typename T>
GenericOpenGLObject<T>::~GenericOpenGLObject()
{
    if( IsActive() ) {
        InternalUnbind();
    }

    InternalDestroy();
}

template<typename T>
bool GenericOpenGLObject<T>::IsActive() const
{
    return BoundObject == objectID;
}

template<typename T>
void GenericOpenGLObject<T>::Bind() const
{
    InternalBind();
    BoundObject = objectID;
}

template<typename T>
void GenericOpenGLObject<T>::Unbind()
{
    InternalUnbind();
    BoundObject = 0u;
}

template<typename T>
void GenericOpenGLObject<T>::SanityCheck()
{
    GLenum errorCode = glGetError();

    dbAssert( errorCode == GL_NO_ERROR, "OpenGL error code ", errorCode, "(", gluErrorString(errorCode), ")" );
}

template<typename T>
GLuint GenericOpenGLObject<T>::BoundObject = 0u;
