/*
 * This file implements a simple wrapper around
 * an OpenGL Vertex Buffer Object (VBO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 13, 2013
 */

#include "Graphics/OpenGL/VertexBufferObject/VertexBufferObject.hpp"

template<>
void GenericOpenGLObject<VertexBufferObject>::InternalCreate()
{
    glGenBuffers( 1, &objectID );
}

template<>
void GenericOpenGLObject<VertexBufferObject>::InternalDestroy() {
    glDeleteBuffers( 1, &objectID );
}

template<>
void GenericOpenGLObject<VertexBufferObject>::InternalBind() const
{
    glBindBuffer( GL_ARRAY_BUFFER, objectID );
}

template<>
void GenericOpenGLObject<VertexBufferObject>::InternalUnbind()
{
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
}
