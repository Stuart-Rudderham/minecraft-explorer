/*
 * This header defines a simple wrapper around
 * an OpenGL Vertex Buffer Object (VBO)
 *
 * Author:      Stuart Rudderham
 * Created:     August 13, 2013
 * Last Edited: August 13, 2013
 */

#ifndef VERTEXBUFFEROBJECT_HPP_INCLUDED
#define VERTEXBUFFEROBJECT_HPP_INCLUDED

#include "Graphics/OpenGL/GenericOpenGLObject.hpp"

class VertexBufferObject : public GenericOpenGLObject<VertexBufferObject> {
    public:

    private:
};

#endif // VERTEXBUFFEROBJECT_HPP_INCLUDED


