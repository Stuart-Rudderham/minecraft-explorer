/*
 * This file implements a wrapper around
 * an OpenGL 2D texture.
 *
 * Author:      Stuart Rudderham
 * Created:     August 3, 2014
 * Last Edited: August 3, 2014
 */

#include "Graphics/OpenGL/Texture/Texture.hpp"

template<>
void GenericOpenGLObject<Texture>::InternalCreate()
{
    glGenTextures( 1, &objectID );
}

template<>
void GenericOpenGLObject<Texture>::InternalDestroy()
{
    glDeleteTextures( 1, &objectID );
}

template<>
void GenericOpenGLObject<Texture>::InternalBind() const
{
    glBindTexture( GL_TEXTURE_2D, objectID );
}

template<>
void GenericOpenGLObject<Texture>::InternalUnbind()
{
    glBindTexture( GL_TEXTURE_2D, 0 );
}
