/*
 * This header defines a wrapper around
 * an OpenGL 2D texture.
 *
 * Author:      Stuart Rudderham
 * Created:     August 3, 2014
 * Last Edited: August 3, 2014
 */

#ifndef TEXTURE_HPP_INCLUDED
#define TEXTURE_HPP_INCLUDED

#include "Graphics/OpenGL/GenericOpenGLObject.hpp"

class Texture : public GenericOpenGLObject<Texture> {
    public:

    private:
};

#endif // TEXTURE_HPP_INCLUDED
