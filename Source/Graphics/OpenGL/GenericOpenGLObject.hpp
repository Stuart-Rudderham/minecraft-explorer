/*
 * This header defines a common base
 * for certain OpenGL objects that have
 * similar create/destroy/bind/unbind functions
 * (e.g. VBO, VAO, shaders, etc...). It
 * provided a common framework while the template
 * specializations provide the specific OpenGL
 * functions to call.
 *
 * Author:      Stuart Rudderham
 * Created:     August 7, 2013
 * Last Edited: August 3, 2014
 */

#ifndef GENERICOPENGLOBJECT_HPP_INCLUDED
#define GENERICOPENGLOBJECT_HPP_INCLUDED

#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

template<typename T>
class GenericOpenGLObject {
    public:
        bool IsActive() const;

        void Bind() const;

        static void Unbind();

    protected:
        GenericOpenGLObject();
        ~GenericOpenGLObject();

        GLuint objectID;

        void SanityCheck();

    private:
        static GLuint BoundObject;

        void InternalCreate();              // These functions will be defined by the
        void InternalDestroy();             // template specializations.
        void InternalBind() const;
        static void InternalUnbind();
};

#include "Graphics/OpenGL/GenericOpenGLObject.inl"

#endif // GENERICOPENGLOBJECT_HPP_INCLUDED
