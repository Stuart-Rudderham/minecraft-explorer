# Controls #

| Action              | Key         |
|---------------------|-------------|
| Move                | WASD        |
| Move Up             | Space       |
| Move Down           | LShift      |
| Look Around         | Mouse       |
| Control Time Of Day | Mouse wheel |
| Quit                | Q           |

# Screenshot #

![](./screenshot.png "Screenshot")
